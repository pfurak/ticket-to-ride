import { animated, useSpring } from 'react-spring'
import disabledImage from '../../../assets/disabled.png'
import styles from './NotAllowed.module.css'

export default function NotAllowed() {
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: 1 },
        config: { duration: 250 }
    })
    return (
        <animated.div style={ style } className='not-allowed' >
            <div className={ styles.notAllowed }>
                <img draggable="false" src={ disabledImage } alt="Not Allowed" />
            </div>
        </animated.div>
    )
}