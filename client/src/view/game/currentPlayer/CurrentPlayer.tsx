import styles from './CurrentPlayer.module.css'
import { Row, Col } from "react-bootstrap"
import { getPlayer } from "../../../state/game/selector"
import { useAppSelector } from "../../../state/store"
import Player from "../players/player/Player"
import Tickets from './tickets/Tickets'
import TrainCards from './trainCards/TrainCards'

export default function CurrentPlayer() {
    const player = useAppSelector(getPlayer)

    return (
        <Row id="current-player-container" className={ styles.currentPlayerContainer }>
            <Col sm={ 2 } style={{ height: "100%" }}>
                <Player player={ player } isOtherPlayer={ false } />
            </Col>

            <Col sm={ 3 } style={{ height: "100%" }}>
                <Tickets tickets={ player.tickets } finishedTickets={ player.finishedTickets } />
            </Col>

            <Col sm={ 7 } style={{ height: "100%" }}>
                <TrainCards cards={ player.cards } />
            </Col>
        </Row>
    )
}