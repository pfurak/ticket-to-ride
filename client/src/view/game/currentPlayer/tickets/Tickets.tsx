import styles from './Tickets.module.css'
import { Button, Row } from "react-bootstrap"
import { cardOrientation } from "../../exports"
import TicketCard, { ticketCardInterface } from "../../ticketCard/TicketCard"
import { useState } from 'react'

interface propsInterface {
    tickets: Array<ticketCardInterface>
    finishedTickets: number
}

export default function Tickets(props: propsInterface) {
    const [showCompletedTickets, setShowCompletedTickets] = useState(false)
    const tickets = props.tickets
    const finishedTickets = props.finishedTickets
    const visibleTickets = tickets.filter((ticket: ticketCardInterface) => ticket.isCompleted === showCompletedTickets)

    const switchButtonText = showCompletedTickets ? "In Progress tickets" : "Completed tickets"
    const switchVisibleTickets: React.MouseEventHandler<HTMLButtonElement> = () => {
        if(finishedTickets > 0) {
            setShowCompletedTickets(!showCompletedTickets)
        }
    }
    return (
        <Row id="player-ticket-container" className={ styles.playerTicketContainer }>
            <Button className={ styles.ticketStateSwitcher } onClick={ switchVisibleTickets }>{ switchButtonText }</Button>

            {
                visibleTickets.map((ticket: ticketCardInterface, index: number) => <TicketCard key={ 'ticket-card-' + ticket.id } ticket={ ticket } orientation={ cardOrientation.PORTRAIT } index={ index } />)
            }
        </Row>
    )
}