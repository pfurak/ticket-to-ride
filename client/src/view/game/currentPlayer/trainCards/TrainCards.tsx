import styles from './TrainCards.module.css'
import { Row } from "react-bootstrap"
import { cardOrientation } from "../../exports"
import TrainCard from '../../trainCard/TrainCard'

interface cardInterface {
    [id: string]: number
}

interface propsInterface {
    cards: cardInterface
}

export default function TrainCards(props: propsInterface) {
    const cards = props.cards
    const types = Object.keys(cards)
    const startLeftPosition = -8
    const leftStep = 11

    return (
        <Row id="player-train-cards-container" className={ styles.playerTrainCardsContainer }>
            {
                types.map((type: string, index: number) => {
                    const quantity = cards[type]
                    const left = startLeftPosition + index * leftStep
                    const card = {
                        id: type,
                        type: type,
                        quantity: quantity,
                        left: left
                    }
                    return (
                        <TrainCard key={ 'player-train-card-' + type + '-' + quantity } card={ card } orientation={ cardOrientation.PORTRAIT } />
                    )
                })
            }
        </Row>
    )
}