import styles from './ScoreTable.module.css'
import { Col, Modal, Row } from "react-bootstrap"
import { sendMessageToServer } from "../../../socket"
import { getGameID, getMyID, getOrderedPlayers } from "../../../state/game/selector"
import { resetState, useAppDispatch, useAppSelector } from "../../../state/store"
import { socketEndPoints, socketParams } from "../../../types/socket"
import ScoredPlayer from './scoredPlayer/ScoredPlayer'

export default function ScoreTable() {
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const myID = useAppSelector(getMyID)
    const orderedPlayers = useAppSelector(getOrderedPlayers)

    const hideModal = () => {
        const params: socketParams = { gameID: gameID, playerID: myID }
        sendMessageToServer(socketEndPoints.LEAVE_ROOM, params, (data: any) => {
            if(data.success === true) {
                dispatch(resetState())
            }
        })
    }

    return (
        <Modal id="game-end-table" show={ true } onHide={ hideModal } animation={ true } backdrop="static" contentClassName={ styles.gameEndModal } dialogClassName={ styles.modal90w }>
            <Modal.Header closeButton>
                <Modal.Title>Game Ended...</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Row className={ styles.playerPointsHeaderInfo }>
                    <Col sm={ 3 }>Player</Col>
                    <Col sm={ 2 }>Score</Col>
                    <Col sm={ 3 }>Tickets</Col>
                    <Col sm={ 2 }>Longest Railway Line</Col>
                    <Col sm={ 2 }>Summed Score</Col>
                </Row>
                {
                    orderedPlayers.map((player: any) => <ScoredPlayer key={ "scored-player-" + player.id } player={ player } />)
                }
            </Modal.Body>
        </Modal>
    )
}