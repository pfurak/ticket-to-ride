import styles from './ScoredPlayer.module.css'
import { useState } from "react"
import { Row, Col, Button } from "react-bootstrap"
import { cardOrientation, colorCodes } from "../../exports"
import { resetPossibleBuildLines, setPossibleBuildLines } from '../../../../state/game/possibleBuildLines/action'
import { useAppDispatch } from '../../../../state/store'
import TicketCard from '../../ticketCard/TicketCard'

interface ticketInterface {
    id: number,
    from: number
    fromCity: string,
    to: number,
    toCity: string,
    point: number,
    isCompleted: boolean,
    railways: Array<number>,
    type: string
}

interface playerInterface {
    id: string,
    name: string,
    color: string,
    hasTheLongestRailLine: boolean,
    longTicket: ticketInterface,
    tickets: Array<ticketInterface>,
    score: number,
    summedPoints: number,
    longestRailLine: Array<number>,
    longestRailLineScore: number,
}

interface propsInterface {
    player: playerInterface
}

export default function ScoredPlayer(props: propsInterface) {
    const dispatch = useAppDispatch()
    const [showTickets, setShowTickets] = useState(false)
    const player = props.player
    const color = colorCodes[player.color]
    const style={ backgroundColor: color }

    let completedTicketPoints = 0
    let notCompletedTicketPoints = 0
    const tickets = [player.longTicket, ...player.tickets]
    tickets.forEach(ticket => {
        if(ticket.isCompleted) {
            completedTicketPoints += ticket.point
        }
        else {
            notCompletedTicketPoints -= ticket.point
        }
    })

    const showHideTickets = () => setShowTickets(!showTickets)
    const showLongestRailLine = () => {
        if(player.longestRailLineScore > 0) {
            dispatch(setPossibleBuildLines(player.longestRailLine))
            document.querySelector<HTMLElement>('#game-end-table')!.style.opacity = "20%"
        }
    }

    const hideLongestRailLine = () => {
        if(player.longestRailLineScore > 0) {
            dispatch(resetPossibleBuildLines())
            document.querySelector<HTMLElement>('#game-end-table')!.style.opacity = "100%"
        }
    }

    return (
        <Row style={ style } className={ styles.playerRow }>
            <Col sm={ 3 }>{ player.name }</Col>
            <Col sm={ 2 }>{ player.score }</Col>
            <Col sm={ 3 }>
                <span>{ completedTicketPoints } / { notCompletedTicketPoints }</span>
                <br />
                <Button variant="info" onClick={ showHideTickets }>Show Tickets</Button>
            </Col>
            <Col sm={ 2 } onMouseEnter={ showLongestRailLine } onMouseLeave={ hideLongestRailLine }>
                { player.longestRailLineScore }
                {
                    player.hasTheLongestRailLine && (
                        " (+10 points)"
                    )
                }
            </Col>
            <Col sm={ 2 }>{ player.summedPoints }</Col>
            {
                showTickets && (
                    <Col sm={ 12 }>
                        <Row>
                            {
                                tickets.map(ticket => {
                                    return (
                                        <Col key={ 'game-ended-ticket-' + ticket.id } sm={ 3 }>
                                            <TicketCard ticket={ ticket } orientation={ cardOrientation.LANDSCAPE } isGameEnded={ true } />
                                        </Col>
                                    )
                                })
                            }
                        </Row>
                    </Col>
                )
            }
        </Row>
    )
}