import styles from './TicketCard.module.css'
import portraitStyles from './Portrait.module.css'
import landscapeStyles from './Landscape.module.css'
import { cardOrientation, colorCodes } from "../exports"
import { useAppDispatch } from '../../../state/store'
import { pinAddedWith, removeFromVisiblePins, setVisiblePins } from '../../../state/game/pin/action'
import { resetPossibleBuildLines, setPossibleBuildLines } from '../../../state/game/possibleBuildLines/action'

export interface ticketCardInterface {
    id: number,
    from: number,
    to: number,
    toCity: string,
    fromCity: string,
    point: number,
    type: string,
    isCompleted: boolean,
    railways: Array<number>
}

interface propsInterface {
    ticket: ticketCardInterface,
    orientation: cardOrientation,
    index?: number
    onClick?: (id: number) => void,
    selected?: boolean,
    isGameEnded?: boolean
}

export default function TicketCard(props: propsInterface) {
    const dispatch = useAppDispatch()
    const ticket = props.ticket
    const orientation = props.orientation
    const isGameEnded = props.isGameEnded
    const pinData = {
        [ticket.from]: pinAddedWith.ON_HOVER,
        [ticket.to]: pinAddedWith.ON_HOVER
    }

    const mouseEnter: React.MouseEventHandler<HTMLDivElement> = () => {
        if(ticket.isCompleted) {
            dispatch(setPossibleBuildLines(ticket.railways))
        }
        dispatch(setVisiblePins(pinData))
        if(isGameEnded) {
            document.querySelector<HTMLElement>('#game-end-table')!.style.opacity = "20%"
        }
    }

    const mouseLeave: React.MouseEventHandler<HTMLDivElement> = () => {
        if(ticket.isCompleted) {
            dispatch(resetPossibleBuildLines())
        }
        dispatch(removeFromVisiblePins(pinData))
        if(isGameEnded) {
            document.querySelector<HTMLElement>('#game-end-table')!.style.opacity = "100%"
        }
    }
    
    let className = styles.ticketCard
    let style = {}
    const onClick = () => {
        if(props.onClick) {
            props.onClick(ticket.id)
        }
    }

    if(orientation === cardOrientation.PORTRAIT) {
        const index = props.index ?? 0
        const startLeftPosition = -10
        const leftStep = 6
        const left = startLeftPosition + index * leftStep

        style = {
            ["--left-position" as any]: left + '%'
        }
        className += ' ' + portraitStyles.ticketCard
    }
    else {
        className += ' ' + landscapeStyles.ticketCard
        if(props.selected) {
            className += ' ' + landscapeStyles.selectedTicketCard
        }

        if(props.isGameEnded) {
            className += ' ' + landscapeStyles.isGameEndedTicket
            if(ticket.isCompleted) {
                style = { ...style, backgroundColor: colorCodes["green"] }
            }
            else {
                style = { ...style, backgroundColor: colorCodes["red"] }
            }
        }
    }

    return (
        <div
            id={ 'player-ticket-' + ticket.id }
            className={ className }
            style={ style }
            onMouseEnter={ mouseEnter }
            onMouseLeave={ mouseLeave }
            onClick={ onClick }
        >
            { ticket.fromCity }<br />
            -<br />
            { ticket.toCity }<br />
            <span>
                { ticket.point }
            </span>
        </div>
    )
}