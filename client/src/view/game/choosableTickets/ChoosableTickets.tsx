import styles from './ChoosableTickets.module.css'
import { Row, Col, Button } from "react-bootstrap"
import { cardOrientation } from "../exports"
import { playerChooseTicket, playerState } from "../../../state/game/action"
import { useAppDispatch, useAppSelector } from "../../../state/store"
import TicketCard, { ticketCardInterface } from "../ticketCard/TicketCard"
import { useState } from 'react'
import { sendMessageToServer } from '../../../socket'
import { playerCoosedTicketsSocketCallbackParams, socketEndPoints } from '../../../types/socket'
import { getCurrentPlayer, getGameID, getPlayer } from '../../../state/game/selector'

interface propsInterface {
    tickets: Array<ticketCardInterface>
}

export default function ChoosableTickets(props: propsInterface) {
    const tickets = props.tickets
    const [selectedTickets, setSelectedTickets] = useState(Array<number>)
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const player = useAppSelector(getPlayer)
    const currentPlayer = useAppSelector(getCurrentPlayer)
    const min = player.numberOfTickets === 1 ? 2 : 1

    const saveSelectedTicket: React.MouseEventHandler<HTMLButtonElement> = () => {
        if(selectedTickets.length < min) {
            return
        }
        const params = { gameID: gameID, playerID: player.id, choosedTickets: selectedTickets }
        sendMessageToServer(socketEndPoints.CHOOSE_TICKETS, params, (data: playerCoosedTicketsSocketCallbackParams) => {
            if(data.success) {
                const newState = player.id === currentPlayer.id ? playerState.WAIT_FOR_ACTION : playerState.WAIT_FOR_TURN
                dispatch(playerChooseTicket(data.ticketDeckSize, { ...data.player, state: newState }, data.history))
            }
        })
    }

    const selectTicket = (id: number) => {
        if(selectedTickets.includes(id)) {
            setSelectedTickets(selectedTickets.filter(ticketID => ticketID !== id))
        }
        else {
            setSelectedTickets([...selectedTickets, id])
        }
    }

    return (
        <Row id="choosable-tickets-container" className={ styles.choosableTicketsContainer }>
            <Col sm={ 12 }>
                <Row>
                    <Col sm={ 12 }>
                        <Button variant="primary" onClick={ saveSelectedTicket }>
                            Choose Tickets ({ selectedTickets.length } / { min })
                        </Button>
                    </Col>
                </Row>

                <Row className={ styles.choosableTickets }>
                    {
                        tickets.map((ticket: ticketCardInterface) => <TicketCard key={ 'ticket-card-' + ticket.id } ticket={ ticket } orientation={ cardOrientation.LANDSCAPE } onClick={ selectTicket } selected={ selectedTickets.includes(ticket.id) } />)
                    }
                </Row>
            </Col>
        </Row>
    )
}