interface colorCodesInterface {
    [id: string]: string
}

export enum cardOrientation {
    LANDSCAPE,
    PORTRAIT
}

export const colorCodes: colorCodesInterface = {
    purple: "purple",
    /* purple: "#c75dc7", */
    white: "white",
    blue: "#42c1f0",
    yellow: "#edd360",
    orange: "#e38932",
    black: "#3a474e",
    red: "#cc2c24",
    green: "#8aad4b",
    joker: "linear-gradient(30deg, purple 12.5%, white 25%, #42c1f0 37.5%, #edd360 50%, #e38932 62.5%, #3a474e 75%, #cc2c24 87.5%, #8aad4b 95%)",
    gray: "#87898e"
}

export const getElementRealPositions = (element: HTMLElement | null | undefined): [left: number, top: number, width: number, height: number] | null => {
    if(element) {
        const rect = element.getBoundingClientRect()
        const left = rect.left
        const top = rect.top
        const width = rect.width
        const height = rect.height
        return [left, top, width, height]
    }
    return null
}

export const getOtherPlayerRealPositions = (playerID: string): Array<number> | null => {
    const playerContainer = document.querySelector<HTMLElement>('#player-container-' + playerID)
    const playerContainerResult = getElementRealPositions(playerContainer)
    if(playerContainer && playerContainerResult) {
        return [...playerContainerResult, playerContainer.getBoundingClientRect().width, playerContainer.getBoundingClientRect().height]
    }
    return null
}

export interface pinInterface {
    id: number,
    city: string,
    x: number,
    y: number
}

export const cityPins: Array<pinInterface> = [{
    id:1,
    city: "Amsterdam",
    x: 31,
    y: 30
}, {
    id: 2,
    city: "Angora",
    x: 85.1,
    y: 89
}, {
    id:3,
    city: "Athina",
    x: 65.3,
    y: 88.75
}, {
    id:4,
    city: "Barcelona",
    x: 20.8,
    y: 83.5
}, {
    id:5,
    city: "Berlin",
    x: 48.5,
    y: 33.25
}, {
    id:6,
    city: "Brest",
    x: 12.5,
    y: 46.5
}, {
    id:7,
    city: "Brindisi",
    x: 53.5,
    y: 77.5
}, {
    id:8,
    city: "Bruxelles",
    x: 29.1,
    y: 36.75
}, {
    id:9,
    city: "Bucuresti",
    x: 72.9,
    y: 63.25
}, {
    id:10,
    city: "Budapest",
    x: 58.5,
    y: 53.25
}, {
    id:11,
    city: "Cadiz",
    x: 10.25,
    y: 92.5
}, {
    id:12,
    city: "Constantinople",
    x: 77.7,
    y:81
}, {
    id:13,
    city: "Danzig",
    x: 59.25,
    y:22.1
}, {
    id:14,
    city: "Dieppe",
    x: 21,
    y:42.25
}, {
    id:15,
    city: "Edinburgh",
    x: 15.75,
    y: 8
}, {
    id:16,
    city: "Erzurum",
    x: 92.75,
    y:85.5
}, {
    id:17,
    city: "Essen",
    x: 39,
    y:31
}, {
    id:18,
    city: "Frankfurt",
    x: 37.5,
    y:41
}, {
    id:19,
    city: "Kharkov",
    x: 91,
    y:47.25
}, {
    id:20,
    city: "Kobenhavn",
    x: 45.75,
    y:16
}, {
    id:21,
    city: "Kyiv",
    x: 78.25,
    y:39.75
}, {
    id:22,
    city: "Lisboa",
    x: 3.9,
    y:85.25
}, {
    id:23,
    city: "London",
    x: 21.75,
    y:29.5
}, {
    id:24,
    city: "Madrid",
    x: 10.25,
    y:82
}, {
    id:25,
    city: "Marseille",
    x: 34.2,
    y:69.75
}, {
    id:26,
    city: "Moskva",
    x: 92.5,
    y:25.5
}, {
    id:27,
    city: "Munchen",
    x: 43.1,
    y:47.5
}, {
    id:28,
    city: "Palermo",
    x: 49,
    y:92.75
}, {
    id:29,
    city: "Pamplona",
    x: 19.75,
    y:70.75
}, {
    id:30,
    city: "Paris",
    x: 25.75,
    y:48.75
}, {
    id:31,
    city: "Petrograd",
    x: 83.2,
    y:8.5
}, {
    id:32,
    city: "Riga",
    x: 67.15,
    y:9.25
}, {
    id:33,
    city: "Roma",
    x: 45.25,
    y:74
}, {
    id:34,
    city: "Rostov",
    x: 95,
    y:54.75
}, {
    id:35,
    city: "Sarajevo",
    x: 60.5,
    y:71.1
}, {
    id:36,
    city: "Sevastopol",
    x: 86,
    y:65.25
}, {
    id:37,
    city: "Smolensk",
    x: 84.25,
    y:29.25
}, {
    id:38,
    city: "Smyrna",
    x: 73.5,
    y:92.25
}, {
    id:39,
    city: "Sochi",
    x: 94.4,
    y:67.5
}, {
    id:40,
    city: "Sofia",
    x: 67,
    y:72.5
}, {
    id:41,
    city: "Stockholm",
    x: 55.75,
    y:5
}, {
    id:42,
    city: "Venezia",
    x: 44.5,
    y:61.2
}, {
    id:43,
    city: "Warszawa",
    x: 64,
    y:32
}, {
    id:44,
    city: "Wien",
    x: 53.75,
    y:49.75
}, {
    id:45,
    city: "Wilno",
    x: 74.6,
    y:28.5
}, {
    id:46,
    city: "Zagrab",
    x: 52.6,
    y:62.8
}, {
    id:47,
    city: "Zurich",
    x: 36.75,
    y:
    56.5
}]

export interface connectionsCarInterface {
    x: number,
    y: number,
    width: number,
    height: number,
    rotate: number,
    train?: boolean
}

export interface connectionsInterface {
    id: number,
    from: number,
    fromCity: string,
    to: number,
    toCity: string,
    color: string,
    locomotive: number,
    selled: boolean,
    enabled: boolean,
    cars: Array<connectionsCarInterface>
}

export interface playerCardsInterface {
    [id: string]: number
}

export const foundPossibleBuildLines = (player: any, lines: Array<connectionsInterface>) => {
    const possibleBuildLines: Array<number> = []
    lines.forEach(line => {
        if(canBuild(line.color, line.locomotive, line.cars.length, player)) {
            possibleBuildLines.push(line.id)
        }
    })
    return possibleBuildLines
}

export const canBuild = (lineType: string, numberOfNeededJokers: number, numberOfCars: number, player: any): Boolean => {
    if(player.wagons < numberOfCars) {
        return false
    }

    const playerCards: playerCardsInterface = player.cards
    const jokerCard = playerCards["joker"] ?? 0
    const lineTypeCard = playerCards[lineType] ?? 0

    if(numberOfNeededJokers > jokerCard) {
        return false
    }

    if(jokerCard >= numberOfCars) {
        return true
    }

    if(lineType === "gray") {
        const keys = Object.keys(playerCards)
        let i = 0
        let possible = false
        while(i < keys.length && !possible) {
            const type = keys[i]
            if(type === "joker") {
                i++
                continue
            }

            possible = playerCards[type] + jokerCard >= numberOfCars
            i++
        }
        return possible
    }
    else {
        if(lineTypeCard + jokerCard >= numberOfCars) {
            return true
        }
    }

    return false
}