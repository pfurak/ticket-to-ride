import styles from './TrainCard.module.css'
import landscapeStyles from './Landscape.module.css'
import portraitStyles from './Portrait.module.css'
import trainImage from '../../../assets/card_train_car.png'
import { Row, Col } from "react-bootstrap"
import { cardOrientation, colorCodes } from "../exports"
import { useAppDispatch, useAppSelector } from '../../../state/store'
import { getGameID, getPlayer } from '../../../state/game/selector'
import NotAllowed from '../notAlloved/NotAllowed'
import { getFirstAction } from '../../../state/action/selector'
import { actionTypes, socketEndPoints } from '../../../types/socket'
import { sendMessageToServer } from '../../../socket'
import { registerAction } from '../../../state/action/action'
import { modifyUpsidedTrainCards, playerState } from '../../../state/game/action'
import { useState } from 'react'
import { resetPossibleBuildLines } from '../../../state/game/possibleBuildLines/action'
import { resetVisiblePins } from '../../../state/game/pin/action'

export interface trainCardInterface {
    id: number | string,
    type: string,
    quantity?: number
    left?: number
    isVariantToBuild?: boolean
}

interface propsInterface {
    card: trainCardInterface,
    orientation: cardOrientation
}

export default function TrainCard(props: propsInterface) {
    const [ onDrag, setOnDrag ] = useState(false)
    const dispatch = useAppDispatch()
    const card: trainCardInterface = props.card
    const orientation: cardOrientation = props.orientation
    const color = colorCodes[card.type]
    const gameID = useAppSelector(getGameID)
    const player = useAppSelector(getPlayer)
    const action = useAppSelector(getFirstAction)
    const isVariantToBuild = card.isVariantToBuild === true
    let isAllowed = player.state === playerState.WAIT_FOR_ACTION || (orientation === cardOrientation.LANDSCAPE && player.state === playerState.DRAW_SECOND_CARD && card.type !== "joker")
    if(isVariantToBuild) {
        isAllowed = true
    }

    if(orientation === cardOrientation.LANDSCAPE) {
        let drawInProgress = false
        const drawFromUpsidedCards: React.MouseEventHandler<HTMLElement> = () => {
            if(!isVariantToBuild && isAllowed && !drawInProgress && !action) {
                drawInProgress = true
                const params = { gameID: gameID, playerID: player.id, action: actionTypes.DRAW_UPSIDED_CARD, otherParams: { cardID: card.id } }
                sendMessageToServer(socketEndPoints.SEND_ACTION, params, (data: any) => {
                    if(data.success) {
                        delete data.success
                        dispatch(resetPossibleBuildLines())
                        dispatch(resetVisiblePins())
                        dispatch(registerAction(data))
                    }
                    else {
                        dispatch(modifyUpsidedTrainCards(data.upsidedTrainCards))
                    }
                })
            }
        }
        const className = styles.card + ' ' + landscapeStyles.landscapeCard + (!isAllowed ? ' ' + landscapeStyles.notAllowed : '') + (isVariantToBuild ? ' ' + landscapeStyles.isVariantToBuild : '')

        return (
            <Row id={ "train-card-" + card.id } className={ className } style={{ background: color }} onClick={ drawFromUpsidedCards } >
                <Col sm={ 12 } md={ 12 } lg={ 12 }>
                    <img src={ trainImage } alt="train_car" /> 
                </Col>
                {
                    !isAllowed && (
                        <NotAllowed />
                    )
                }
            </Row>
        )
    }

    const quantity = card.quantity ?? 1
    const left = card.left ?? -10
    const style: React.CSSProperties = {
        background: color,
        left: left + '%'
        //    ['--left' as any]: left + '%'
    }
    const className = styles.card + ' ' + portraitStyles.portraitCard + (!isAllowed ? ' ' + portraitStyles.notAllowed : '')
    const spanClassName = onDrag ? portraitStyles.noRotate : ''
    
    const dragStart = () => {
        if(quantity > 1) {
            setOnDrag(true)
        }
    }

    const dragEnd = () => {
        if(quantity > 1) {
            setOnDrag(false)
        }
    }

    return (
        <Row id={ "player-train-card-" + card.id } className={ className } style={ style } draggable onDragStart={ dragStart } onDragEnd={ dragEnd } data-target={ card.type }>
            <Col sm={ 12 } md={ 12 } lg={ 12 }>
                <img src={ trainImage } alt="train_car" />
                {
                    quantity > 1 && (
                        <span className={ spanClassName }>{ quantity }</span>
                    )
                }
                {
                    !isAllowed && (
                        <NotAllowed />
                    )
                }
            </Col>
        </Row>
    )
}