import styles from './Game.module.css'
import { useEffect, useState } from "react"
import { sendMessageToServer, subscribeToEvent, unSubscribeFromEvent } from "../../socket"
import { GameState, setGameData } from "../../state/game/action"
import { getGameID, getGameState, getHistory, getMyID } from "../../state/game/selector"
import { useAppDispatch, useAppSelector } from "../../state/store"
import { actionTypes, socketEndPoints, socketEvents } from "../../types/socket"
import { Col, Row } from 'react-bootstrap'
import Map from './map/Map'
import TicketDeck from './ticketDeck/TicketDeck'
import UpsidedTrainCards from './upsidedTrainCards/UpsidedTrainCards'
import History from './history/History'
import CurrentPlayer from './currentPlayer/CurrentPlayer'
import Deck from './deck/Deck'
import LeftSideBar from './leftSideBar/LeftSideBar'
import { registerAction } from '../../state/action/action'
import ScoreTable from './scoreTable/ScoreTable'

export default function Game() {
    const [loading, setLoading] = useState(true)
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const myID = useAppSelector(getMyID)
    const history = useAppSelector(getHistory)
    const gameEnded = useAppSelector(getGameState)

    useEffect(() => {
        const parameters = { gameID: gameID, playerID: myID }
        sendMessageToServer(socketEndPoints.GET_GAME_DATA, parameters, (data) => {
            if(data.success && data.gameData) {
                dispatch(setGameData(data.gameData))
                setLoading(false)
            }
        })

        subscribeToEvent(socketEvents.GET_ACTION, (data: any) => dispatch(registerAction(data)) )
        subscribeToEvent(socketEvents.ADD_TURN, (data: any) => {
            dispatch(registerAction({
                action: actionTypes.ADD_TURN,
                currentPlayerID: data.currentPlayerID
            }))
        })

        subscribeToEvent(socketEvents.GAME_ENDED, (data: any) => {
            dispatch(registerAction({
                action: actionTypes.GAME_ENDED,
                orderedPlayers: data.orderedPlayers
            }))
        })

        return(() => unSubscribeFromEvent([socketEvents.GET_ACTION, socketEvents.ADD_TURN, socketEvents.GAME_ENDED]))
    }, [gameID, myID, dispatch])

    return (
        <div className={ styles.gameContainer }>
            { !loading && (
                <div>
                    <Row>
                        <Col sm={2} style={{ position: "relative" }}>
                            <LeftSideBar />
                        </Col>

                        <Col sm={8}>
                            <Map />
                        </Col>

                        <Col sm={2}>
                            <TicketDeck />
                            <UpsidedTrainCards />
                        </Col>
                    </Row>

                    <Row>
                        <Col sm={2}>
                            <History key={ history.length } />
                        </Col>
                        
                        <Col sm={8}>
                            <CurrentPlayer />
                        </Col>

                        <Col sm={2}>
                            <Deck />
                        </Col>
                    </Row>

                    {
                        gameEnded === GameState.ENDED_GAME && (
                            <ScoreTable />
                        )
                    }
                </div>
            )}
        </div>
    )
}