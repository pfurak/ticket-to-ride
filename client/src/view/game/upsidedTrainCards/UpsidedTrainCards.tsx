import styles from './UpsidedTrainCards.module.css'
import { getPlayer, getUpsidedTrainCards } from '../../../state/game/selector'
import { useAppDispatch, useAppSelector } from '../../../state/store'
import TrainCard, { trainCardInterface } from '../trainCard/TrainCard'
import { cardOrientation } from '../exports'
import { useEffect } from 'react'
import { getFirstAction } from '../../../state/action/selector'
import { drawUpsidedTrainCard, playerState } from '../../../state/game/action'
import { actionTypes } from '../../../types/socket'
import { animate } from './animate'
import { removeAction } from '../../../state/action/action'

export default function UpsidedTrainCards() {
    const dispatch = useAppDispatch()
    const upsidedTrainCards = useAppSelector(getUpsidedTrainCards)
    const player = useAppSelector(getPlayer)
    const action = useAppSelector(getFirstAction)

    useEffect(() => {
        const enabledState = [ playerState.WAIT_FOR_TURN, playerState.WAIT_FOR_ACTION, playerState.DRAW_SECOND_CARD ]
        if(enabledState.includes(player.state)) {
            if(action && action.animate === actionTypes.DRAW_UPSIDED_CARD) {
                animate(player.id, action.animateCaller, action.upsidedCardID, action.cardTypeFromDeck, () => {
                    dispatch(removeAction(action.id))
                    const afterAnimate = action.afterAnimate
                    dispatch(drawUpsidedTrainCard(afterAnimate.upsidedTrainCards, afterAnimate.desckSize, afterAnimate.history, afterAnimate.player))
                })
            }
        }
    }, [player, action, dispatch])

    return (
        <div id="upsided-train-cards" className={ styles.upsidedTrainCards }>
            {
                upsidedTrainCards.map((trainCard: trainCardInterface) => <TrainCard key={ 'upsided-train-card-' + trainCard.id } card={ trainCard } orientation={ cardOrientation.LANDSCAPE } />)
            }
        </div>
    )
}