import { colorCodes, getElementRealPositions, getOtherPlayerRealPositions } from "../exports"

const ANIMATE_DURATION = 2000

export const animate = async (myID: string, animateCallerID: string, upsidedCardID: number, cardTypeFromDeck: string, callback: () => void) => {
    const deckCardAnimatePromise = animateDeckCard(cardTypeFromDeck)
    const trainCardAnimatePromise = myID === animateCallerID ? animateCardToMe(upsidedCardID) : animateToOtherPlayer(animateCallerID, upsidedCardID)
    await Promise.all([deckCardAnimatePromise, trainCardAnimatePromise])
    callback()
}

const animateCardToMe = (upsidedCardID: number) => {
    const container = document.querySelector<HTMLElement>('#player-train-cards-container')
    const result = getElementRealPositions(container)
    if(result) {
        const [left, top] = result
        const rotate = -90
        const width = '74%'
        const options = {
            top: top / 2,
            left: left,
            transform: 'rotate(' + rotate + 'deg)',
            width: width,
            marginLeft: '0%'
        }
        return animateUpsidedTrainCard(options, upsidedCardID, true)
    }
}

const animateToOtherPlayer = (animateCallerID: string, upsidedCardID: number) => {
    const playerContainerResult = getOtherPlayerRealPositions(animateCallerID)
    if(playerContainerResult) {
        const [playerContainerLeft, playerContainerTop, playerContainerWidth, playerContainerHeight] = playerContainerResult
        const top = playerContainerTop - playerContainerHeight / 4
        const left = playerContainerLeft + playerContainerWidth / 2
        const options = {
            top: top,
            left: left
        }
        return animateUpsidedTrainCard(options, upsidedCardID)
    }
}

interface animateInterface {
    top: number,
    left: number,
    transform?: string,
    width?: string,
    marginLeft?: string
}

const animateUpsidedTrainCard = (options: animateInterface, upsidedCardID: number, animateToMe: boolean = false) => {
    return new Promise((resolve) => {
        const card = document.querySelector<HTMLElement>('#upsided-train-cards')?.querySelector<HTMLElement>('#train-card-' + upsidedCardID)
        const cardResult = getElementRealPositions(card)
        if(card && cardResult) {
            const [cardLeft, cardTop] = cardResult
            options.left -= cardLeft
            if(animateToMe) {
                options.top += card.getBoundingClientRect().height * 2.5
            }
            else {
                options.top -= card.getBoundingClientRect().height
            }
    
            card.style.transition = "none"
            card.style.position = "absolute"
            card.style.top = (cardTop / 2) + 'px'
            card.style.left = '0px'
            card.style.zIndex = '1'
            if(animateToMe && options.marginLeft) {
                card.style.marginLeft = options.marginLeft
            }
            card.animate([{
                ...options,
                top: options.top + 'px',
                left: options.left + 'px',
                opacity: 0
            }], {
                duration: ANIMATE_DURATION
            }).onfinish = () => {
                card.style.transition = "none"
                card.style.position = "absolute"
                card.style.top = options.top + 'px'
                card.style.left = options.left + 'px'
                card.style.opacity = '0'
                if(options.transform) {
                    card.style.transform = options.transform
                }
    
                if(options.width) {
                    card.style.width = options.width
                }
    
                if(options.marginLeft) {
                    card.style.marginLeft = options.marginLeft
                }

                resolve(true)
            }
        }
    })
}

const animateDeckCard = (cardTypeFromDeck: string) => {
    return new Promise((resolve) => {
        if(cardTypeFromDeck) {
            const card = document.querySelector<HTMLElement>('#deck-container')?.querySelector<HTMLElement>('#card-1')
            const cardResult = getElementRealPositions(card)

            const lastUpsidedTrainCardContainer = document.querySelector('#upsided-train-cards')
            let top = lastUpsidedTrainCardContainer?.getBoundingClientRect().top ?? 0
            if(lastUpsidedTrainCardContainer) {
                const lastUpsidedTrainCard = Array.from(lastUpsidedTrainCardContainer.querySelectorAll('div')).at(-1)
                if(lastUpsidedTrainCard) {
                    top = lastUpsidedTrainCard.getBoundingClientRect().top
                }
            }
            
            const duration = ANIMATE_DURATION / 4
            if(card && cardResult) {
                const [, cardTop] = cardResult
                top = (cardTop - top) / -2
                top -= 10

                card.animate([{
                    top: top + 'px',
                    transform: 'rotateX(90deg)',
                }], {
                    duration: duration
                }).onfinish = () => {
                    card.style.top = top + 'px'
                    card.style.transform = 'rotateX(90deg)'

                    const color = colorCodes[cardTypeFromDeck]
                    card.style.background = color
                    card.style.justifyContent = "center"
                    card.style.padding = "8% 0"
                    card.style.border = "none"

                    card.querySelectorAll('.question-mark')?.forEach(elem => elem.remove())

                    const image = card.querySelector('img')
                    if(image) {
                        image.style.transform = 'rotate(180deg)'
                    }

                    top *= 2
                    card.animate([{
                        top: top + 'px',
                        transform: 'rotateX(180deg)',
                    }], {
                        duration: duration
                    }).onfinish = () => {
                        card.style.top = top + 'px'
                        card.style.transform = 'rotateX(180deg)'

                        resolve(true)
                    }
                }
            }
        }
        else {
            resolve(true)
        }
    })
}