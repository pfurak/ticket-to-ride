import styles from './History.module.css'
import { animated, useSpring } from "react-spring"
import { useAppSelector } from '../../../state/store'
import { getHistory } from '../../../state/game/selector'
import { historyInterface } from '../../../state/game/reducer'
import { colorCodes } from '../exports'

export default function History() {
    const histories: Array<historyInterface> = useAppSelector(getHistory)
    const duration = 750
    const length = histories.length
    const lastHistory = length > 0 ? histories[length - 1] : ""
    const penultimateHistory = length > 1 ? histories[length - 2] : ""
    const penultimatePenultimateHistory = length > 2 ? histories[length - 3] : ""

    const lastHistoryStyle = useSpring({
        from: { opacity: 0.05, transform: "translate(0px, -20px)" },
        to: { opacity: 1, transform: "translate(0px, 20px)" },
        config: { duration: duration }
    })

    const penultimateHistoryStyle = useSpring({
        from: { transform: "translate(0px, 20px)" },
        to: { transform: "translate(0px, 40px)" },
        config: { duration: duration }
    })

    const penultimatePenultimateHistoryStyle = useSpring({
        from: { opacity: 1, transform: "translate(0px, 40px) rotateX(0deg)" },
        to: { opacity: 0, transform: "translate(0px, 55px) rotateX(-90deg)" },
        config: { duration: duration }
    })

    return (
        <div id="history" className={ styles.historyContainer }>
            <p>History</p>

            <animated.div style={ lastHistoryStyle }>
                <GenerateHistory history={ lastHistory } />
            </animated.div>
    

            <animated.div style={ penultimateHistoryStyle }>
                <GenerateHistory history={ penultimateHistory } />
            </animated.div>


            <animated.div style={ penultimatePenultimateHistoryStyle }>
                <GenerateHistory history={ penultimatePenultimateHistory } />
            </animated.div>
        </div>
    )
}

interface propsInterface {
    history: historyInterface | string
}
function GenerateHistory(props: propsInterface) {
    const history = props.history
    if(typeof history === "string") {
        return (
            <p></p>
        )
    }
    
    const color = colorCodes[history.colorCode]
    const text = history.playerName + ' ' + history.text
    return (
        <p style={{ color: color }}>{ text }</p>
    )
}