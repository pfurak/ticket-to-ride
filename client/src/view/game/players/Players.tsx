import styles from './Players.module.css'
import { getOtherPlayers } from "../../../state/game/selector"
import { useAppSelector } from "../../../state/store"
import Player from './player/Player'

export default function Players() {
    const otherPlayers = useAppSelector(getOtherPlayers)

    return (
        <div id="other-players-container" className={ styles.otherPlayersContainer }>
            {
                otherPlayers.map((player: any) => {
                    return (
                        <Player key={ "other-player-" + player.id } player={ player } isOtherPlayer={ true } />
                    )
                })
            }
        </div>
    )
}