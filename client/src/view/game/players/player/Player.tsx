import styles from './Player.module.css'
import trainCarImage from '../../../../assets/train.png'
import cardImage from '../../../../assets/card.png'
import starImage from '../../../../assets/star.png'
import goalsImage from '../../../../assets/goals.png'

import { Col, Row } from 'react-bootstrap'
import { colorCodes } from '../../exports'
import { playerState } from '../../../../state/game/action'
import { useAppDispatch } from '../../../../state/store'
import { resetPossibleBuildLines, setPossibleBuildLines } from '../../../../state/game/possibleBuildLines/action'
import NotAllowed from '../../notAlloved/NotAllowed'

interface playerInterface {
    id: string,
    color: string,
    name: string,
    finishedTickets: number,
    numberOfCards: number,
    numberOfTickets: number,
    score: number,
    wagons: number
    state: playerState
    longestRailLineScore: number,
    longestRailLine: Array<number>
}

interface propsInterface {
    player: playerInterface,
    isOtherPlayer: boolean,
}

export default function Player(props: propsInterface) {
    const dispatch = useAppDispatch()
    const player = props.player
    const isOtherPlayer = props.isOtherPlayer
    const longestRailLineScore = player.longestRailLineScore
    const longestRailLine = player.longestRailLine
    const color = colorCodes[player.color]
    const containerStyle = { backgroundColor: color, borderColor: color }

    const showLongestRailLines = () => {
        if(longestRailLineScore > 0) {
            dispatch(setPossibleBuildLines(longestRailLine))
        }
    }

    const hideLongestRailLines = () => dispatch(resetPossibleBuildLines())

    if(isOtherPlayer) {
        return (
            <div id={ "player-container-" + player.id } className={ styles.playerContainer } style={ containerStyle }>
                <Row className={ styles.playerHeader }>
                    <Col sm={ 6 }>
                        { player.name }
                        {
                            longestRailLineScore > 0 && (
                                <span title='Longest Railline(s)' onMouseEnter={ showLongestRailLines } onMouseLeave={ hideLongestRailLines }> (Lr: { longestRailLineScore })</span>
                            )
                        }
                    </Col>
    
                    <Col sm={ 3 }>
                        <img src={ trainCarImage } alt="train_car" title="Wagon(s)" /> 
                    </Col>
    
                    <Col sm={ 3 }>
                        { player.wagons }
                    </Col>
                </Row>

                <Row className={ styles.playerBody }>
                    <Col sm={ 4 }>
                        <img src={ cardImage } alt="card" title="Card(s)" /> 
                    </Col>
    
                    <Col sm={ 4 }>
                        <img src={ starImage } alt="star" title="Score(s)" /> 
                    </Col>
    
                    <Col sm={ 4 }>
                        <img src={ goalsImage } alt="goals" title="Ticket(s): In Progress / Completed" /> 
                    </Col>
                </Row>
        
                <Row className={ styles.playerBody }>
                    <Col sm={ 4 }>
                        { player.numberOfCards }
                    </Col>
    
                    <Col sm={ 4 }>
                        { player.score }
                    </Col>
    
                    <Col sm={ 4 }>
                        { player.numberOfTickets- player.finishedTickets }  / { player.finishedTickets }
                    </Col>
                </Row>
                
                {
                    player.state === playerState.LEFT_GAME && (
                        <NotAllowed />
                    )
                }
            </div>
        )
    }

    return (
        <div id={ "player-container-" + player.id } className={ styles.playerContainer + ' ' + styles.currentPlayerContainer } style={ containerStyle }>
            <Row className={ styles.playerHeader }>
                <Col sm={ 12 }>
                    { player.name }
                    {
                        longestRailLineScore > 0 && (
                            <span title='Longest Railline(s)' onMouseEnter={ showLongestRailLines } onMouseLeave={ hideLongestRailLines }> (Lr: { longestRailLineScore })</span>
                        )
                    }
                </Col>
            </Row>

            <Row className={ styles.playerBody }>
                <Col sm={ 4 }>
                    <img src={ trainCarImage } alt="train_car" title="Wagon(s)" /> 
                </Col>

                <Col sm={ 8 }>
                    { player.wagons }
                </Col>
            </Row>

            <Row className={ styles.playerBody }>
                <Col sm={ 4 }>
                    <img src={ cardImage } alt="card" title="Card(s)" /> 
                </Col>

                <Col sm={ 8 }>
                    { player.numberOfCards }
                </Col>
            </Row>

            <Row className={ styles.playerBody }>
                <Col sm={ 4 }>
                    <img src={ starImage } alt="star" title="Score(s)" /> 
                </Col>

                <Col sm={ 8 }>
                    { player.score }
                </Col>
            </Row>

            <Row className={ styles.playerBody }>
                <Col sm={ 4 }>
                    <img src={ goalsImage } alt="goals" title="Ticket(s): In Progress / Completed" /> 
                </Col>

                <Col sm={ 8 }>
                    { player.numberOfTickets- player.finishedTickets }  / { player.finishedTickets }
                </Col>
            </Row>
        </div>
    )
}