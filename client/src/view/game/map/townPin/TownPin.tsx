import styles from './TownPin.module.css'
import { foundPossibleBuildLines, pinInterface } from '../../exports'
import { useAppDispatch, useAppSelector } from '../../../../state/store'
import { getVisiblePins } from '../../../../state/game/pin/selector'
import { getPlayer, getRailLines } from '../../../../state/game/selector'
import { modifyPlayer, playerState } from '../../../../state/game/action'
import { pinAddedWith, resetVisiblePins, setVisiblePins } from '../../../../state/game/pin/action'
import { resetPossibleBuildLines, setPossibleBuildLines } from '../../../../state/game/possibleBuildLines/action'
import { getPossibleBuildLines } from '../../../../state/game/possibleBuildLines/selector'

interface propsInterface {
    pin: pinInterface
}
export default function TownPin(props: propsInterface) {
    const dispatch = useAppDispatch()
    const player = useAppSelector(getPlayer)
    const railLines = useAppSelector(getRailLines)
    const possibleBuildLines = useAppSelector(getPossibleBuildLines)
    const visiblePins: Array<number> = useAppSelector(getVisiblePins)
    const pin = props.pin
    const selectedPin = visiblePins.includes(pin.id)
    const className = styles.pin + (selectedPin ? ' ' + styles.selectedPin: '')

    const onClick: React.MouseEventHandler<HTMLDivElement> = () => {
        if(player.state === playerState.WAIT_FOR_ACTION) {
            if(selectedPin) {
                dispatch(resetVisiblePins())
                dispatch(resetPossibleBuildLines())
            }
            else {
                const pinData = { [pin.id]: pinAddedWith.ON_CLICK }
                if(visiblePins.length === 0) {
                    const lines = railLines.filter(line => line.enabled && !line.selled && (line.from === pin.id || line.to === pin.id))
                    if(lines.length > 0) {
                        const possibleBuildLines = foundPossibleBuildLines(player, lines)
                        if(possibleBuildLines.length > 0) {
                            dispatch(setVisiblePins(pinData))
                            dispatch(setPossibleBuildLines(possibleBuildLines))
                        }
                    }
                }
                else {
                    const searchResult = railLines.filter(line => possibleBuildLines.includes(line.id) && (line.from === pin.id || line.to === pin.id))
                    if(searchResult.length === 0) {
                        dispatch(resetVisiblePins())
                        dispatch(resetPossibleBuildLines())
                        const lines = railLines.filter(line => line.enabled && !line.selled && (line.from === pin.id || line.to === pin.id))
                        if(lines.length > 0) {
                            const foundedPossibleBuildLines = foundPossibleBuildLines(player, lines)
                            if(foundedPossibleBuildLines.length > 0) {
                                dispatch(setVisiblePins(pinData))
                                dispatch(setPossibleBuildLines(foundedPossibleBuildLines))
                            }
                        }
                    }
                    else {
                        const foundedPossibleBuildLines: Array<number> = []
                        searchResult.forEach(line => foundedPossibleBuildLines.push(line.id))
                        dispatch(setPossibleBuildLines(foundedPossibleBuildLines))
                        dispatch(setVisiblePins(pinData))
                        dispatch(modifyPlayer({
                            ...player,
                            state: playerState.CHOOSE_CARD_VARIANT_TO_BUILD
                        }))
                    }
                }
            }
        }
    }

    return (
        <div id={ 'pin-' + pin.id } className={ className } style={{
            top: pin.y + '%',
            left: pin.x + '%'
        }} onClick={ onClick }></div>
    )
}