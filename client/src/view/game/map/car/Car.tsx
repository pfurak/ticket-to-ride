import styles from './Car.module.css'
import { connectionsInterface, connectionsCarInterface, colorCodes, canBuild } from "../../exports"
import { useAppDispatch, useAppSelector } from '../../../../state/store'
import { getPossibleBuildLines } from '../../../../state/game/possibleBuildLines/selector'
import { getPlayer, getRailLines } from '../../../../state/game/selector'
import { setPossibleBuildLines } from '../../../../state/game/possibleBuildLines/action'
import { modifyPlayer, playerState } from '../../../../state/game/action'

interface propsInterface {
    connection: connectionsInterface
}

export default function Car(props: propsInterface): any {
    const dispatch = useAppDispatch()
    const railLines = useAppSelector(getRailLines)
    const possibleBuildLines = useAppSelector(getPossibleBuildLines)
    const player = useAppSelector(getPlayer)
    const connection = props.connection
    const isPossibleBuildLine = !connection.selled && connection.enabled && possibleBuildLines.includes(connection.id)
    const isTicketLine = connection.selled && possibleBuildLines.includes(connection.id)
    const color = colorCodes[connection.color]
    let opacity = isPossibleBuildLine ? 1 : (connection.selled ? 1 : 0)

    const handleOnDragOver: React.DragEventHandler<HTMLDivElement> = e => {
        e.stopPropagation()
        e.preventDefault()

        if(!connection.selled && connection.enabled && !possibleBuildLines.includes(connection.id) && canBuild(connection.color, connection.locomotive, connection.cars.length, player)) {
            dispatch(setPossibleBuildLines([...possibleBuildLines, connection.id]))
        }
    }

    const handleOnDragLeave: React.DragEventHandler<HTMLDivElement> = () => {
        dispatch(setPossibleBuildLines(possibleBuildLines.filter(lineID => lineID !== connection.id)))
    }

    const handleOnDrop: React.DragEventHandler<HTMLDivElement> = () => {
        const railLine = railLines.find(line => line.id === connection.id)
        if(railLine) {
            const pins = [railLine.from, railLine.to]
            const possibleLines = railLines.filter(line => pins.includes(line.from) && pins.includes(line.to) && canBuild(line.color, line.locomotive, line.cars.length, player)).map(line => line.id)
            if(possibleLines.length > 0) {
                dispatch(setPossibleBuildLines(possibleLines))
                dispatch(modifyPlayer({
                    ...player,
                    state: playerState.CHOOSE_CARD_VARIANT_TO_BUILD
                }))
            }
        }
    }

    return (
        connection.cars.map((car: connectionsCarInterface, index: number) => {
            const key = "connection-" + connection.id + "-" + index
            const id = "car-" + connection.id + "-" + index
            let className = styles.car + " connection-" + connection.id
            if(isPossibleBuildLine) {
                className += ' ' + styles.availableConnectionCar
            }
            if(isTicketLine) {
                className += ' ' + styles.completedTicketConnectionCar
            }
            const isTran = car.train ?? false

            return (
                <div
                    key={ key }
                    id={ id }
                    className={ className }
                    onDragOver={ handleOnDragOver }
                    onDragLeave={ handleOnDragLeave }
                    onDrop={ handleOnDrop }
                    style={{
                        backgroundColor: color,
                        opacity: opacity,
                        left: car.x + '%',
                        top: car.y + '%',
                        width: car.width + "%",
                        height: car.height + "%",
                        transform: "rotate(" + car.rotate + "deg)",
                    }}
                >
                    {
                        isTran && (
                            <hr />
                        )
                    }
                </div>
            )
        })
    )
}