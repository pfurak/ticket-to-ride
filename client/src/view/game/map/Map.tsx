import styles from './Map.module.css'
import Car from './car/Car'
import TownPin from './townPin/TownPin'
import { useEffect, useState } from 'react'
import { removeAction } from '../../../state/action/action'
import { getFirstAction } from '../../../state/action/selector'
import { build, playerState } from '../../../state/game/action'
import { getPlayer, getRailLines } from '../../../state/game/selector'
import { useAppDispatch, useAppSelector } from '../../../state/store'
import { actionTypes } from '../../../types/socket'
import { cityPins, pinInterface } from '../exports'
import { animate } from './animate'
import { getVisiblePins } from '../../../state/game/pin/selector'
import { getPossibleBuildLines } from '../../../state/game/possibleBuildLines/selector'

export default function Map() {
    const [overlay, setOverlay] = useState(false)
    const dispatch = useAppDispatch()
    const railLines = useAppSelector(getRailLines)
    const player = useAppSelector(getPlayer)
    const action = useAppSelector(getFirstAction)
    const visiblePins: Array<number> = useAppSelector(getVisiblePins)
    const possibleBuildLines = useAppSelector(getPossibleBuildLines)
    const needOverlay = visiblePins.length > 0 || possibleBuildLines.length > 0
    const overlayedMap = styles.overlayMap + (needOverlay || overlay ? ' ' + styles.showOverlay : '')

    useEffect(() => {
        const enabledState = [ playerState.WAIT_FOR_TURN, playerState.BUILD ]
        if(enabledState.includes(player.state)) {
            if(action && action.animate === actionTypes.BUILD) {
                setOverlay(true)
                animate(player.id, action.animateCaller, action.animateCallerColor, action.railLineID, action.railLineCarNumbers, () => {
                    dispatch(removeAction(action.id))
                    const afterAnimate = action.afterAnimate
                    dispatch(build(
                        afterAnimate.desckSize,
                        afterAnimate.history,
                        afterAnimate.player,
                        afterAnimate.railLines,
                        afterAnimate.upsidedTrainCards
                    ))
                    setOverlay(false)
                })
            }
        }
    }, [player, action, dispatch, setOverlay])

    return (
        <div id="map-container" className={ styles.mapContainer }>
            <div className={ overlayedMap }></div>

            {
                cityPins.map((pin: pinInterface) => <TownPin key={ 'city-pin-' + pin.id } pin={ pin } />)
            }

            {
                railLines.map(connection => {
                    return (
                        <Car key={ 'connection-' + connection.id } connection={ connection } />
                    )
                })
            }
        </div>
    )
}