import buildSound from '../../../assets/build.wav'
import styles from './car/Car.module.css'
import { colorCodes, getElementRealPositions } from "../exports"

const ANIMATE_DELAY = 250
const ANIMATE_DURATION = 2000

export const animate = async (myID: string, animateCallerID: string, animateCallerColor: string, railLineID: number, railLineCarNumbers: number, callback: () => void) => {
    if(process.env.REACT_APP_LOBBY_MUSIC === "true") {
        const audio = new Audio(buildSound)
        audio.volume = 0.8 // 80%
        audio.play()
    }

    let top = '100%'
    let left = '0%' 
    let rotate = 0

    if(myID === animateCallerID) {
        const result = getElementRealPositions(document.querySelector<HTMLElement>('#player-container-' + animateCallerID)?.querySelector<HTMLElement>('img'))
        if(result) {
            const [, imgTop, imgWidth, imgHeight] = result
            left = imgWidth + 'px'
            top = (imgTop - imgHeight / 2) + 'px'
            rotate = -45
        }
    }
    else {
        // If user choosed ticket and the player box animation didn't finished, we need to wait that
        await new Promise(resolve => setTimeout(() => resolve(true), 500))

        const result = getElementRealPositions(document.querySelector<HTMLElement>('#player-container-' + animateCallerID)?.querySelector<HTMLElement>('img'))
        if(result) {
            const [imgLeft, imgTop, , imgHeight] = result
            left = (imgLeft / -2) + 'px'
            top = (imgTop + imgHeight / 2) + 'px'
        }
    }
    const promises: Array<Promise<boolean>> = []

    for(let i = 0; i < railLineCarNumbers; i++) {
        promises.push(animateCar(top, left, rotate, animateCallerColor, railLineID, i))
    }

    await Promise.all(promises)
    callback()
}

const animateCar = (top: string, left: string, rotate: number, animateCallerColor: string, railLineID: number, carNumber: number) => {
    return new Promise<boolean>(resolve => {
        const id = "car-" + railLineID + "-" + carNumber
        const car = document.querySelector<HTMLElement>('#' + id)
        if(car) {
            const toTop = car.style.top
            const toLeft = car.style.left
            const toTransform = car.style.transform

            car.style.transition = "none"
            car.style.top = top
            car.style.left = left
            car.style.transform = 'rotate(' + rotate + 'deg)'
            car.style.backgroundColor = colorCodes[animateCallerColor]
            car.style.opacity = '1'
            car.classList.add(styles.animatedCar)

            const delay = ANIMATE_DELAY * carNumber
            setTimeout(() => {
                car.animate([{
                    top: toTop,
                    left: toLeft,
                    transform: toTransform
                }], {
                    duration: ANIMATE_DURATION
                }).onfinish = () => {
                    car.style.transition = "none"
                    car.style.top = toTop
                    car.style.left = toLeft
                    car.style.transform = toTransform
                    car.classList.remove(styles.animatedCar)
                    resolve(true)
                }
            }, delay)
        }
        else {
            resolve(true)
        }
    })
}