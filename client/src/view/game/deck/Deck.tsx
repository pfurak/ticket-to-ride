import styles from './Deck.module.css'
import { getDeckSize, getGameID, getPlayer } from '../../../state/game/selector'
import { useAppDispatch, useAppSelector } from '../../../state/store'
import Card from './card/Card'
import { getFirstAction } from '../../../state/action/selector'
import { actionTypes, socketEndPoints } from '../../../types/socket'
import { sendMessageToServer } from '../../../socket'
import { registerAction, removeAction } from '../../../state/action/action'
import { drawCardFromDeck, modifyDeckSize, playerState } from '../../../state/game/action'
import { useEffect } from 'react'
import { animate } from './animate'
import { resetPossibleBuildLines } from '../../../state/game/possibleBuildLines/action'
import { resetVisiblePins } from '../../../state/game/pin/action'

export default function Deck() {
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const player = useAppSelector(getPlayer)
    const deckSize = useAppSelector(getDeckSize)
    const action = useAppSelector(getFirstAction)
    const cards: Array<JSX.Element> = []
    const isAllowed = [ playerState.WAIT_FOR_ACTION, playerState.DRAW_SECOND_CARD_FROM_DECK ].includes(player.state)

    for(let i = 0; i < deckSize; i++) {
        cards.push(<Card
            key={ 'deck-card-' + i + '-' + Math.ceil(Math.random() * 1000) }
            id={ i }
            zIndex={ deckSize - i }
            isAllowed={ isAllowed }
        />)
    }

    let drawInProgress = false
    const drawCard: React.MouseEventHandler<HTMLDivElement> = () => {
        if(isAllowed && !drawInProgress && !action) {
            drawInProgress = true
            const params = { gameID: gameID, playerID: player.id, action: actionTypes.DRAW_CARD_FROM_DECK }
            sendMessageToServer(socketEndPoints.SEND_ACTION, params, (data: any) => {
                if(data.success) {
                    delete data.success
                    dispatch(resetPossibleBuildLines())
                    dispatch(resetVisiblePins())
                    dispatch(registerAction(data))
                }
                else {
                    dispatch(modifyDeckSize(data.desckSize))
                }
            })
        }
    }

    useEffect(() => {
        const enabledState = [ playerState.WAIT_FOR_TURN, playerState.WAIT_FOR_ACTION, playerState.DRAW_SECOND_CARD_FROM_DECK ]
        if(enabledState.includes(player.state)) {
            if(action && action.animate === actionTypes.DRAW_CARD_FROM_DECK) {
                animate(player.id, action.animateCaller, action.cardTypeFromDeck, () => {
                    const afterAnimate = action.afterAnimate
                    dispatch(removeAction(action.id))
                    dispatch(drawCardFromDeck(afterAnimate.desckSize, afterAnimate.history, afterAnimate.player))
                })
            }
        }
    }, [ player, action, dispatch])
    
    return (
        <div id="deck-container" className={ styles.deckContainer } onClick={ drawCard }>
            {
                cards.map(card => card)
            }
        </div>
    )
}