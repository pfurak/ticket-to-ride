import trainImage from '../../../../assets/card_train_car.png'
import styles from './Card.module.css'
import { Col, Row } from 'react-bootstrap'
import NotAllowed from '../../notAlloved/NotAllowed'

interface propsInterface {
    id: number,
    zIndex: number
    isAllowed: boolean
}

export default function Card(props: propsInterface) {
    const id = props.id
    const zIndex = props.zIndex
    const isAllowed = props.isAllowed
    const defaultTop = 0
    const topStep = 0.125
    const top = defaultTop + id * topStep

    const style: React.CSSProperties = {
        top: top + 'vh',
        zIndex: zIndex,
        ["--cursor" as any]: isAllowed ? 'pointer' : 'not-allowed'
    }

    return (
        <Row id={ 'card-' + (id + 1) } className={ styles.card } style={ style }>
            <Col sm={ 2 } className='question-mark'>
                ?
            </Col>

            <Col sm={ 8 }>
                <img src={ trainImage } alt="train_image" />
            </Col>

            <Col sm={ 2 } className='question-mark'>
                ?
            </Col>
            {
                !isAllowed && (
                    <NotAllowed />
                )
            }
        </Row>
    )
}