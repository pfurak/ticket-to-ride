import { colorCodes, getElementRealPositions, getOtherPlayerRealPositions } from "../exports"

const ANIMATE_DURATION = 2000

export const animate = (myID: string, animateCallerID: string, cardTypeFromDeck: string, callback: () => void) => {
    if(myID === animateCallerID) {
        animateCardToMe(cardTypeFromDeck, callback)
    }
    else {
        animateToOtherPlayer(animateCallerID, callback)
    }
}

const animateCardToMe = (cardTypeFromDeck: string, callback: () => void) => {
    const container = document.querySelector<HTMLElement>('#player-train-cards-container')
    const containerResult = getElementRealPositions(container)
    const card = document.querySelector<HTMLElement>('#deck-container')?.querySelector<HTMLElement>('#card-1')
    const cardResult = getElementRealPositions(card)
    if(containerResult && card && cardResult) {
        const [containerResultLeft, containerResultTop ] = containerResult
        const [cardLeft, cardTop ] = cardResult
        const top = Math.ceil((cardTop - containerResultTop) * 4) + 2 // +2 because of border left and right
        const left = containerResultLeft - cardLeft
        const halfLeft = left / 2
        let transform = 'rotateY(90deg) rotate(-90deg)'
        const width = '74%'

        const duration = ANIMATE_DURATION / 2
        card.style.boxShadow = "none"
        card.animate([{
            top: top + 'px',
            left: halfLeft + 'px',
            transform: transform,
            width: width
        }], {
            duration: duration
        }).onfinish = () => {
            card.style.top = top + 'px'
            card.style.left = halfLeft + 'px'
            card.style.transform = transform
            card.style.width = width

            card.querySelectorAll('.question-mark')?.forEach(elem => elem.remove())
            const image = card.querySelector('img')
            if(image) {
                image.style.transform = 'rotate(180deg)'
            }

            const color = colorCodes[cardTypeFromDeck]
            card.style.background = color
            card.style.justifyContent = "center"
            card.style.padding = "8% 0"
            card.style.border = "none"

            transform = 'rotateY(180deg) rotate(-90deg)'

            card.animate([{
                left: left + 'px',
                transform: transform,
                opacity: '0'
            }], {
                duration: duration
            }).onfinish = () => {
                card.style.left = left + 'px'
                card.style.transform = transform
                card.style.opacity = '0'

                callback()
            }
        }
    }
    else {
        callback()
    }
}

const animateToOtherPlayer = (animateCallerID: string, callback: () => void) => {
    const playerContainerResult = getOtherPlayerRealPositions(animateCallerID)
    const card = document.querySelector<HTMLElement>('#deck-container')?.querySelector<HTMLElement>('#card-1')
    const cardResult = getElementRealPositions(card)
    if(playerContainerResult && card && cardResult) {
        const [playerContainerLeft, playerContainerTop, playerContainerWidth, playerContainerHeight] = playerContainerResult
        let top = playerContainerTop - playerContainerHeight / 2
        let left = playerContainerLeft + playerContainerWidth / 2

        const [cardLeft, cardTop] = cardResult
        left -= cardLeft
        top -= card.getBoundingClientRect().height - cardTop
        top *= -1

        card.animate([{
            top: top + 'px',
            left: left + 'px',
            transform: 'rotateX(0deg)',
            opacity: '0'
        }], {
            duration: ANIMATE_DURATION
        }).onfinish = () => {
            card.style.top = top + 'px'
            card.style.left = left + 'px'
            card.style.opacity = '0'
            card.style.transform = 'rotateX(0deg)'
            
            callback()
        }
    }
    else {
        callback()
    }
}