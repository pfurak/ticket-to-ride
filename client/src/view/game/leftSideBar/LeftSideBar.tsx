import { useAppSelector, useAppDispatch } from "../../../state/store"
import { addTurn, gameEnded, playerChooseTicket, playerState, removePlayer } from "../../../state/game/action"
import { getPlayer } from "../../../state/game/selector"
import GameInfo from '../gameInfo/GameInfo'
import Players from '../players/Players'
import ChoosableTickets from '../choosableTickets/ChoosableTickets'
import { useSpringRef, useTransition, animated } from 'react-spring'
import { useEffect } from "react"
import { subscribeToEvent, unSubscribeFromEvent } from "../../../socket"
import { actionTypes, playerCoosedTicketsSocketCallbackParams, socketEvents } from "../../../types/socket"
import { getFirstAction } from "../../../state/action/selector"
import { removeAction } from "../../../state/action/action"
import CardVariants from "./cardVariants/CardVariants"

export default function LeftSideBar() {
    const dispatch = useAppDispatch()
    const transRef = useSpringRef()
    const player = useAppSelector(getPlayer)
    const action = useAppSelector(getFirstAction)
    const playersVisible = [playerState.WAIT_FOR_TURN, playerState.WAIT_FOR_ACTION, playerState.DRAW_SECOND_CARD, playerState.DRAW_SECOND_CARD_FROM_DECK, playerState.BUILD].includes(player.state)
    const views = [
        <>
            <GameInfo />
            <Players />
        </>
    ]
    if(player.state === playerState.CHOOSE_TICKET) {
        views.push(
            <ChoosableTickets tickets={ player.choosableTickets } />
        )
    }

    if(player.state === playerState.CHOOSE_CARD_VARIANT_TO_BUILD) {
        views.push(<CardVariants />)
    }
    
    const from = playersVisible
    ? { opacity: 0, transform: 'translate3d(-100%,0,0)' }
    : { opacity: 0, transform: 'translate3d(100%,0,0)' }
    const enter = playersVisible
    ? { opacity: 1, transform: 'translate3d(0%,0,0)' }
    : { opacity: 1, transform: 'translate3d(0%,0,0)' }
    const leave = playersVisible
    ? { opacity: 0, transform: 'translate3d(100%,0,0)' }
    : { opacity: 0, transform: 'translate3d(-100%,0,0)' }

    const transition = useTransition(playersVisible ? 0 : 1, {
        ref: transRef,
        keys: null,
        from: from,
        enter: enter,
        leave: leave,
        config: { duration: 500 }
    })

    useEffect(() => {
        subscribeToEvent(socketEvents.PLAYER_CHOOSED_TICKETS, (data: playerCoosedTicketsSocketCallbackParams) => {
            dispatch(playerChooseTicket(data.ticketDeckSize, data.player, data.history))
        })

        subscribeToEvent(socketEvents.PLAYER_LEFT_ROOM, data => {
            const playerID = data.playerID
            dispatch(removePlayer(playerID))
        })

        if(action) {
            if(action.action === actionTypes.ADD_TURN) {
                dispatch(removeAction(action.id))
                dispatch(addTurn(action.currentPlayerID))
            }

            if(action.action === actionTypes.GAME_ENDED) {
                dispatch(removeAction(action.id))
                dispatch(gameEnded(action.orderedPlayers))
            }
        }

        return(() => unSubscribeFromEvent([ socketEvents.PLAYER_CHOOSED_TICKETS, socketEvents.PLAYER_LEFT_ROOM ]))
    }, [action, dispatch])

    useEffect(() => {
        transRef.start()
    }, [playersVisible, transRef])

    return (
        <div style={{ position: "absolute", width: "100%", height: "100%", zIndex: 9 }}>
            {
                transition((style, index) => {
                    return (
                        <animated.div style={ style }>
                            <div style={{
                                position: "absolute",
                                top: 0,
                                width: "100%",
                                height: "100%",
                                paddingLeft: "12px",
                                paddingRight: "12px"
                            }}>
                                { views[index] }
                            </div>
                        </animated.div>
                    )
                })
            }
        </div>
    )
}