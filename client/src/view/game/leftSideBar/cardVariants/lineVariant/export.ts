import { playerCardsInterface } from "../../../exports";

export const getCardVariantToBuild = (lineType: string, numberOfNeededJokers: number, numberOfCars: number, playerCards: playerCardsInterface) => {
    let cardVariants: Array<playerCardsInterface> = []
    const jokerCards = playerCards["joker"] ?? 0

    if(numberOfNeededJokers > jokerCards) {
        return cardVariants
    }

    if(jokerCards >= numberOfCars) {
        cardVariants.push({ joker: numberOfCars })
    }

    if(lineType === "gray") {
        for(const key in playerCards) {
            if(key === "joker") {
                continue
            }
            cardVariants = [...cardVariants, ...getCardVariant(key, numberOfNeededJokers, numberOfCars, playerCards)]
        }
    }
    else {
        cardVariants = [...cardVariants, ...getCardVariant(lineType, numberOfNeededJokers, numberOfCars, playerCards)]
    }
    
    return cardVariants
}

const getCardVariant = (lineType: string, numberOfNeededJokers: number, numberOfCars: number, playerCards: playerCardsInterface) => {
    const variants: Array<playerCardsInterface> = []
    let jokerCards = playerCards["joker"] ?? 0
    const lineTypeCard = playerCards[lineType] ?? 0
    let countTo = numberOfNeededJokers - 1

    if(numberOfNeededJokers === 0) {
        if(lineTypeCard >= numberOfCars) {
            variants.push({
                [lineType]: numberOfCars
            })
        }

        countTo = 0
    }
    
    while(jokerCards > countTo) {
        const result = numberOfCars - jokerCards
        if(result > 0 && result <= lineTypeCard) {
            variants.push({
                joker: jokerCards,
                [lineType]: result
            })
        }
        jokerCards--
    }

    return variants
}