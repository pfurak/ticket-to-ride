import styles from './LineVariant.module.css'
import { Button, Col, Row } from "react-bootstrap"
import { getGameID, getPlayer } from "../../../../../state/game/selector"
import { useAppDispatch, useAppSelector } from "../../../../../state/store"
import { cardOrientation, colorCodes, connectionsInterface, playerCardsInterface } from "../../../exports"
import { getCardVariantToBuild } from "./export"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faXmark } from '@fortawesome/free-solid-svg-icons'
import TrainCard from '../../../trainCard/TrainCard'
import { actionTypes, socketEndPoints } from '../../../../../types/socket'
import { sendMessageToServer } from '../../../../../socket'
import { modifyPlayer, modifyRailLines, playerState } from '../../../../../state/game/action'
import { resetPossibleBuildLines } from '../../../../../state/game/possibleBuildLines/action'
import { resetVisiblePins } from '../../../../../state/game/pin/action'
import { registerAction } from '../../../../../state/action/action'

interface propsInterface {
    line: connectionsInterface
}

export default function LineVariant(props: propsInterface) {
    const player = useAppSelector(getPlayer)
    const railLine = props.line
    const color = colorCodes[ railLine.color ]
    const cardVariants = getCardVariantToBuild(railLine.color, railLine.locomotive, railLine.cars.length, player.cards)
    return (
        <Row className={ styles.lineVariantContainer }>
            <p style={{ color: color }}>{ railLine.fromCity } - {railLine.toCity}</p>
            {
                cardVariants.map((variant, key) => <CardVariant key={ 'card-variants-' + railLine.id + '-' + key } railLineID={ railLine.id } cards={ variant } />)
            }
        </Row>
    )
}

interface cardVariantPropsInterface {
    railLineID: number,
    cards: playerCardsInterface
}
function CardVariant(props: cardVariantPropsInterface) {
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const player = useAppSelector(getPlayer)
    const railLineID = props.railLineID
    const cards = props.cards
    const handleOnClick = () => {
        const params = { gameID: gameID, playerID: player.id, action: actionTypes.BUILD, otherParams: { railLineID: railLineID, cards: cards } }
        sendMessageToServer(socketEndPoints.SEND_ACTION, params, (data: any) => {
            dispatch(resetPossibleBuildLines())
            dispatch(resetVisiblePins())
            if(data.success) {
                delete data.success
                dispatch(modifyPlayer({
                    ...player,
                    state: playerState.BUILD
                }))
                dispatch(registerAction(data))
            }
            else {
                dispatch(modifyRailLines(data.railLines))
                if(data.player) {
                    dispatch(modifyPlayer(data.player))
                }
            }
        })
        
    }
    return (
        <Row className={ styles.cardVariantContainer }>
            <Button variant='success' onClick={ handleOnClick }><FontAwesomeIcon icon={ faCheck } /></Button>
            {
                Object.keys(cards).map((cardType: string) => {
                    const card = { id: Math.random(), type: cardType, isVariantToBuild: true }
                    return (
                        <div key={ 'line-card-' + railLineID + '-' + cardType } style={{ width: '100%', display: 'flex' }}>
                            <Col sm={ 9 } md={ 9 } lg={ 9 }>
                                <TrainCard orientation={ cardOrientation.LANDSCAPE } card={ card } />
                            </Col>
        
                            <Col sm={ 3 } md={ 3 } lg={ 3 }>
                                <FontAwesomeIcon icon={ faXmark } /> { cards[cardType] }
                            </Col>
                        </div>
                    )
                })
            }
            <hr />
        </Row>
    )
}