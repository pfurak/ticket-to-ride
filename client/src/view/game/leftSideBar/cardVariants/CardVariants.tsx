import styles from './CardVariants.module.css'
import { Button, Row } from 'react-bootstrap'
import { useAppDispatch, useAppSelector } from '../../../../state/store'
import { getPlayer, getRailLines } from '../../../../state/game/selector'
import { getPossibleBuildLines } from '../../../../state/game/possibleBuildLines/selector'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark } from '@fortawesome/free-solid-svg-icons'
import { resetVisiblePins } from '../../../../state/game/pin/action'
import { resetPossibleBuildLines } from '../../../../state/game/possibleBuildLines/action'
import { modifyPlayer, playerState } from '../../../../state/game/action'
import LineVariant from './lineVariant/LineVariant'

export default function CardVariants() {
    const dispatch = useAppDispatch()
    const player = useAppSelector(getPlayer)
    const possibleBuildLines = useAppSelector(getPossibleBuildLines)
    const railLines = useAppSelector(getRailLines)
    const possibleLines = railLines.filter(line => possibleBuildLines.includes(line.id))

    const resetBuild: React.MouseEventHandler<HTMLButtonElement> = () => {
        dispatch(resetVisiblePins())
        dispatch(resetPossibleBuildLines())
        dispatch(modifyPlayer({
            ...player,
            state: playerState.WAIT_FOR_ACTION
        }))
    }

    return (
        <Row id="card-variants-container" className={ styles.cardVariantsContainer }>
            <Button variant='danger' className={ styles.cancelBuildButton } onClick={ resetBuild }>
                <FontAwesomeIcon icon={ faXmark } />
            </Button>

            {
                possibleLines.map(line => <LineVariant key={ 'line-variant-' + line.id } line={ line } />)
            }
        </Row>
    )
}

