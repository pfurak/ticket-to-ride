import styles from './GameInfo.module.css'
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Button, Row, Col } from "react-bootstrap"
import { getCurrentPlayer, getGameID, getMyID } from "../../../state/game/selector"
import { resetState, useAppDispatch, useAppSelector } from "../../../state/store"
import { colorCodes } from '../exports'
import { socketEndPoints, socketParams } from '../../../types/socket'
import { sendMessageToServer } from '../../../socket'

export default function GameInfo() {
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const myID = useAppSelector(getMyID)
    const currentPlayer = useAppSelector(getCurrentPlayer)

    const leaveGame: React.MouseEventHandler<HTMLButtonElement> = () => {
        const params: socketParams = { gameID: gameID, playerID: myID }
        sendMessageToServer(socketEndPoints.LEAVE_ROOM, params, (data: any) => {
            if(data.success === true) {
                dispatch(resetState())
            }
        })
    }
    const color = colorCodes[currentPlayer.color]

    return (
        <Row className={ styles.gameInfo }>
            <Col sm={ 2 }>
                <Button variant="danger" onClick={ leaveGame }>
                    <FontAwesomeIcon icon={ faSignOutAlt } />
                </Button>
            </Col>

            <Col sm={ 10 }>
                <span style={{ color: color }}>{ currentPlayer.name }'s turn</span>
            </Col>
        </Row>
    )
}