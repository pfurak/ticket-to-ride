import NotAllowed from '../../notAlloved/NotAllowed'
import styles from './Card.module.css'

interface propsInterface {
    id: number,
    zIndex: number
    isAllowed: boolean
}

export default function Card(props: propsInterface) {
    const id = props.id
    const zIndex = props.zIndex
    const isAllowed = props.isAllowed
    const defaultTop = 0
    const topStep = 0.25
    const top = defaultTop + id * topStep

    const style: React.CSSProperties = {
        top: top + 'vh',
        zIndex: zIndex,
        ["--cursor" as any]: isAllowed ? 'pointer' : 'not-allowed'
    }

    return (
        <div id={ 'ticket-card-' + (id + 1) } className={ styles.ticketCard } style={ style }>
            Ticket
            {
                !isAllowed && (
                    <NotAllowed />
                )
            }
        </div>
    )
}