import { getElementRealPositions, getOtherPlayerRealPositions } from "../exports"

const ANIMATE_DELAY = 250
const ANIMATE_DURATION = 2000

export const animate = (myID: string, animateCallerID: string, animatedCardCounter: number, callback: () => void) => {
    if(myID === animateCallerID) {
        animateCardsToMe(animatedCardCounter, callback)
    }
    else {
        animateToOtherPlayer(animateCallerID, animatedCardCounter, callback)
    }
}

const animateCardsToMe = (animatedCardCounter: number, callback: () => void) => {
    const ticketButton = document.querySelector<HTMLElement>('#player-ticket-container')?.querySelector<HTMLElement>('button')
    const buttonResult = getElementRealPositions(ticketButton)
    if(ticketButton && buttonResult) {
        const [buttonLeft, buttonTop] = buttonResult
        const top = buttonTop + ticketButton.offsetTop / 1.8
        const left = buttonLeft + ticketButton.getBoundingClientRect().width
        const width = '74%'
        const rotate = -90
        for(let i = 0; i < animatedCardCounter; i++) {
            setTimeout(() => {
                const options = {
                    top: top,
                    left: left,
                    transform: 'rotate(' + rotate + 'deg)',
                    width: width
                }
                animateCard(options, i, i === animatedCardCounter - 1, callback, true)
            }, i * ANIMATE_DELAY)
        }
    }
}

const animateToOtherPlayer = (animateCallerID: string, animatedCardCounter: number, callback: () => void) => {
    const playerContainerResult = getOtherPlayerRealPositions(animateCallerID)
    if(playerContainerResult) {
        const [playerContainerLeft, playerContainerTop, playerContainerWidth] = playerContainerResult
        const top = playerContainerTop
        const left = playerContainerLeft + playerContainerWidth
        for(let i = 0; i < animatedCardCounter; i++) {
            setTimeout(() => {
                const options = {
                    top: top,
                    left: left
                }
                animateCard(options, i, i === animatedCardCounter - 1, callback)
            }, i * ANIMATE_DELAY)
        }
    }
}

interface animateInterface {
    top: number,
    left: number,
    transform?: string,
    width?: string
}

const animateCard = (options: animateInterface, cardNumber: number, lastCard: boolean, callback: () => void, recalculateLeftPosition: boolean = false) => {
    const card = document.querySelector<HTMLElement>('#ticket-deck-container')?.querySelector<HTMLElement>('#ticket-card-' + (cardNumber + 1))
    const cardResult = getElementRealPositions(card)
    if(card && cardResult) {
        const [cardLeft, cardTop] = cardResult
        options.top += cardTop
        options.left -= cardLeft
        if(recalculateLeftPosition && cardNumber > 0) {
            options.left += cardNumber * (card.getBoundingClientRect().width / 2)
        }
        
        card.animate([{
            ...options,
            opacity: 0.25,
            top: options.top + 'px',
            left: options.left + 'px'
        }], {
            duration: ANIMATE_DURATION
        }).onfinish = () => {
            card.style.opacity = '0'
            card.style.top = options.top + 'px'
            card.style.left = options.left + 'px'
            if(options.transform) {
                card.style.transform = options.transform
            }

            if(options.width) {
                card.style.width = options.width
            }

            if(lastCard) {
                callback()
            }
        }
    }
}