import styles from './TicketDeck.module.css'
import { getGameID, getPlayer, getTicketDeckSize } from "../../../state/game/selector"
import { useAppDispatch, useAppSelector } from "../../../state/store"
import Card from './card/Card'
import { sendMessageToServer } from '../../../socket'
import { actionTypes, socketEndPoints } from '../../../types/socket'
import { registerAction, removeAction } from '../../../state/action/action'
import { useEffect } from 'react'
import { modifyPlayer, modifyTicketDeckSize, playerState } from '../../../state/game/action'
import { getFirstAction } from '../../../state/action/selector'
import { animate } from './animate'
import { resetPossibleBuildLines } from '../../../state/game/possibleBuildLines/action'
import { resetVisiblePins } from '../../../state/game/pin/action'

export default function TicketDeck() {
    const dispatch = useAppDispatch()
    const gameID = useAppSelector(getGameID)
    const player = useAppSelector(getPlayer)
    const action = useAppSelector(getFirstAction)
    const ticketDeckSize = useAppSelector(getTicketDeckSize)
    const isAllowed = player.state === playerState.WAIT_FOR_ACTION
    const cards: Array<JSX.Element> = []

    for(let i = 0; i < ticketDeckSize; i++) {
        cards.push(<Card
            key={ 'ticket-deck-card-' + i + '-' + Math.ceil(Math.random() * 1000) }
            id={ i }
            zIndex={ ticketDeckSize - i }
            isAllowed={ isAllowed }
        />)
    }

    let drawInProgress = false
    const drawTicket: React.MouseEventHandler<HTMLDivElement> = () => {
        if(isAllowed && !drawInProgress && !action) {
            drawInProgress = true
            const params = { gameID: gameID, playerID: player.id, action: actionTypes.DRAW_TICKET }
            sendMessageToServer(socketEndPoints.SEND_ACTION, params, (data: any) => {
                if(data.success) {
                    delete data.success
                    dispatch(resetPossibleBuildLines())
                    dispatch(resetVisiblePins())
                    dispatch(registerAction(data))
                }
            })
        }
    }

    useEffect(() => {
        const enabledState = [ playerState.WAIT_FOR_TURN, playerState.WAIT_FOR_ACTION ]
        if(enabledState.includes(player.state)) {
            if(action && action.animate === actionTypes.DRAW_TICKET) {
                animate(player.id, action.animateCaller, action.animatedCardCounter, () => {
                    dispatch(removeAction(action.id))
                    dispatch(modifyTicketDeckSize(action.afterAnimate.ticketDeckSize))
                    if(action.afterAnimate.player) {
                        dispatch(modifyPlayer(action.afterAnimate.player))
                    }
                })
            }
        }
    }, [player, action, dispatch])

    return (
        <div id="ticket-deck-container" className={ styles.ticketDeckContainer } onClick={ drawTicket }>
            {
                cards.map(card => card)
            }
        </div>
    )
}