import trainImage from '../../../../assets/card_train_car.png'
import styles from '../../../game/trainCard/TrainCard.module.css'
import { Col, Row } from 'react-bootstrap'
import { colorCodes } from "../../../game/exports"

interface propsInterface {
    colorCode: string
}

export default function Card(props: propsInterface) {
    const colorCode = props.colorCode
    const color = colorCodes[colorCode]

    return (
        <Row className={ styles.card } style={{ background: color }} >
            <Col sm={ 12 } md={ 12 } lg={ 12 }>
                <img src={ trainImage } alt="train_car" /> 
            </Col>
        </Row>
    )
}