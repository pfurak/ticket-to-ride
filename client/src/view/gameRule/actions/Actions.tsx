import styles from './Actions.module.css'
import upsidedTrainCardsImage from '../../../assets/game-rule/upsided-train-cards.png'
import ticketDeckImage from '../../../assets/game-rule/ticket-deck.png'
import deckImage from '../../../assets/game-rule/deck.png'
import doubleRoutesImage from '../../../assets/game-rule/double-routes.png'
import parallelRoutesImage from '../../../assets/game-rule/parallel-routes.png'
import firstFerriesImage from '../../../assets/game-rule/ferries-1.png'
import { Col, Row } from 'react-bootstrap'
import Card from './card/Card'

export default function Actions() {
    return (
        <section className={ styles.section }>
            <h2>Actions</h2>
            <Row className='align-items-center'>
                <Col sm={ 12 }>
                    <p>A player must perform one (and only one) of the following four actions</p>
                </Col>
            </Row>

            <Row className='align-items-center'>
                <Col sm={ 3 } className="text-right">
                    <img src={ upsidedTrainCardsImage } alt="Upsided Train Cards" title="Upsided Train Cards" className={ styles.upsidedTrainCardsImage } />                    
                </Col>

                <Col sm={ 3 } className="text-left">
                    Draw 2 Train cards or 1 Joker card from the upsided cards
                    <br />
                    <i>Note: If the number of joker cards in the upsided cards are more than 2 the game discard the upsided train cards and displays new cards. If the new cards still have more than 2 joker cards the game do this step again. Only do this step 3 times</i>
                </Col>

                <Col sm={ 6 }>
                    <Row className='align-items-center'>
                        <Col sm={ 6 } className="text-right">
                            Draw Ticket from Ticket Deck
                            <br />
                            <i>Note: At the beginning of the game, player must have choose 2 ticket. During the game it's enough if player choose only 1 ticket</i>
                        </Col>

                        <Col sm={ 6 } className="text-left">
                            <img src={ ticketDeckImage } alt="Ticket Deck" title="Ticket Deck" />
                        </Col>
                    </Row>

                    <br />
                    <br />

                    <Row className='align-items-center'>
                        <Col sm={ 6 } className="text-right">
                            <img src={ deckImage } alt="Deck" title="Deck" />
                        </Col>

                        <Col sm={ 6 } className="text-left">
                            Draw 2 Card from Deck
                        </Col>
                    </Row>
                </Col>
            </Row>
                
            <Row className='align-items-center justify-content-center'>
                <Col sm={ 10 }>
                    Building
                    <p>A route is a set of continuous colored spaces (includes gray spaces too) between two adjacent cities on the map. To claim a route, a player must play a set of Train cards whose color and quantity match the color and number of spaces of the chosen route.</p>
                    <Row className='justify-content-between'>
                        <Col sm={ 5 } className={ styles.exampleContainer }>
                            Most routes require a specific set of colored cards to claim them. Joker cards can always act as a stand-in for any given color
                            <br />
                            Player want to build a railway that requires 3 blue card
                            <br />
                            Possible cards
                            <Row>
                                <Col sm={ 4 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='blue' />
                                </Col>
                            </Row>

                            <Row>
                                <Col sm={ 4 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='joker' />
                                </Col>
                            </Row>

                            <Row>
                                <Col sm={ 4 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='joker' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='joker' />
                                </Col>
                            </Row>

                            <Row>
                                <Col sm={ 4 }>
                                    <Card colorCode='joker' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='joker' />
                                </Col>

                                <Col sm={ 4 }>
                                    <Card colorCode='joker' />
                                </Col>
                            </Row>
                        </Col>

                        <Col sm={ 5 } className={ styles.exampleContainer }>
                            Routes that are Gray can be claimed using a set of cards of any one color
                            <br />
                            Player want to build a railway that requires 2 car and its color is gray
                            <br />
                            Possible cards

                            <Row className='justify-content-between'>
                                <Col sm={ 5 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 5 }>
                                    <Card colorCode='blue' />
                                </Col>
                            </Row>

                            <Row className='justify-content-between'>
                                <Col sm={ 5 }>
                                    <Card colorCode='blue' />
                                </Col>

                                <Col sm={ 5 }>
                                    <Card colorCode='joker' />
                                </Col>
                            </Row>

                            <Row className='justify-content-between'>
                                <Col sm={ 5 }>
                                    <Card colorCode='red' />
                                </Col>

                                <Col sm={ 5 }>
                                    <Card colorCode='red' />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>

            <Row className='align-items-center justify-content-center'>
                <Col sm={ 10 }>
                    <p>Double routes</p>

                    <Row className='align-items-center'>
                        <Col sm={ 4 } className="text-right">
                            Some cities are connected by Double-Routes. These are routes whose spaces are parallel and equal in number from one city to the other
                        </Col>

                        <Col sm={ 2 } className="text-left">
                            <img src={ doubleRoutesImage } alt="Double Routes" title="Double Routes" className={ styles.routesImage } />
                        </Col>

                        <Col sm={ 4 } className="text-right">
                            Be aware of routes that are partially parallel to each other but are linked to different cities. These are not double-routes
                        </Col>

                        <Col sm={ 2 } className="text-left">
                            <img src={ parallelRoutesImage } alt="Double Routes" title="Double Routes" className={ styles.routesImage } />
                        </Col>
                    </Row>

                    <p><i>Note: In two or three player games, only one of the Double-Routes can be used. A player can claim either of the two routes between cities, but the other route is then closed to the other players for the remainder of the game</i></p>
                </Col>
            </Row>

            <Row className='align-items-center justify-content-center'>
                <Col sm={ 10 }>
                    <p>Ferries</p>

                    <Row className='align-items-center justify-content-center'>
                        <Col sm={ 2 } className="text-right">
                            <img src={ firstFerriesImage } alt="Ferries" title="Ferries" className={ styles.routesImage } />
                        </Col>

                        <Col sm={ 8 } className="text-left">
                            Ferries are special Gray routes linking two adjacent cities across a body of water. They are easily identified by the Locomotive icon(s) featured on at least one of the spaces making the route.
                        </Col>
                    </Row>

                    <p>To claim a Ferry Route, a player must play a Locomotive card for each Locomotive symbol on the route, and the usual set of cards of the proper color for the remaining spaces of that Ferry Route.</p>
                </Col>
            </Row>
        </section>
    )
}