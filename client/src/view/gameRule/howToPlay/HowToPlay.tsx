import { Col, Row } from 'react-bootstrap'
import styles from './HowToPlay.module.css'
import drawTicketImage from '../../../assets/game-rule/draw-ticket.png'
import clickOnPinImage from '../../../assets/game-rule/click-on-pin.png'
import dragAndDropImage from '../../../assets/game-rule/drag-and-drop.png'
import buildPossibilitiesImage from '../../../assets/game-rule/build-possibilities.png'

export default function HowToPlay() {
    return (
        <section className={ styles.section }>
            <h2>How To Play</h2>

            <Row className='align-items-center'>
                <Col sm={ 12 }>
                    <p>By clicking, the player can draw a train card from the deck or from the upsided train cards</p>
                </Col>
            </Row>

            <Row className='align-items-center'>
                <Col sm={ 8 }>
                    By clicking on the Ticket Deck, the card draw animation starts. When the animation is finished, the player box(es) disappear(s) and the choosable tickets will be shown on the same place
                </Col>

                <Col sm={ 4 } className='text-left'>
                    <img src={ drawTicketImage } alt="Draw Ticket" title="Draw Ticket" className={ styles.drawTicketImage } />
                </Col>
            </Row>

            <Row className='align-items-center justify-content-center'>
                <Col sm={ 11 }>
                    <p>To build just click on one of the pin of the selected route or drag-and-drop the card on the route</p>

                    <Row className='justify-content-between'>
                        <Col sm={ 6 }>
                            <p>Clicking on a pin</p>
                            The game check the card of the player and show the possible routes (if have)
                            <img src={ clickOnPinImage } alt="Click on Pin" title="Click on Pin" className={ styles.clickOnPinImage } />
                            When possible routes appear just click the second pin of the selected route
                        </Col>

                        <Col sm={ 6 }>
                            <p>Drag and Drop</p>
                            The game check the card of the player and show the possible route (if have)
                            <img src={ dragAndDropImage } alt="Drag and Drop" title="Drag and Drop" className={ styles.clickOnPinImage } />
                            If the possible route appear just drop the card on that
                        </Col>
                    </Row>

                    <p>After one of these steps the player box(es) disappear(s) and the build possibilities will be shown on the same place</p>
                    <img src={ buildPossibilitiesImage } alt="Build Possibilities" title="Build Possibilities" />
                    
                    <p>Clicking on the green button will start to building the route</p>
                </Col>
            </Row>
        </section>
    )
}