import { Col, Row } from 'react-bootstrap'
import styles from './Scoring.module.css'

export default function Scoring() {
    return (
        <section className={ styles.section }>
            <h2>Calculating Scores</h2>

            <Row className='justify-content-center'>
                <Col sm={ 12 }>
                    The value of successfully completed tickets is added to their total score. The value for any incomplete Tickets is deducted from their total score
                </Col>
            </Row>

            <Row className='align-items-center justify-content-center'>
                <Col sm={ 3 }>
                    <p>Score of route</p>
                    <Row className='align-items-center'>
                        <Col sm={ 6 } className="text-left">Route Length</Col>
                        <Col sm={ 6 } className="text-right">Points Scored</Col>

                        <Col sm={ 6 } className="text-left">1</Col>
                        <Col sm={ 6 } className="text-right">1</Col>

                        <Col sm={ 6 } className="text-left">2</Col>
                        <Col sm={ 6 } className="text-right">2</Col>

                        <Col sm={ 6 } className="text-left">3</Col>
                        <Col sm={ 6 } className="text-right">4</Col>

                        <Col sm={ 6 } className="text-left">4</Col>
                        <Col sm={ 6 } className="text-right">7</Col>

                        <Col sm={ 6 } className="text-left">6</Col>
                        <Col sm={ 6 } className="text-right">15</Col>

                        <Col sm={ 6 } className="text-left">8</Col>
                        <Col sm={ 6 } className="text-right">21</Col>
                    </Row>
                </Col>
            </Row>

            <p>+10 point bonus for the European Express to the player(s) who have the Longest Continuous Path on the board. When evaluating and comparing path lengths, only take into account continuous lines of trains of the same color</p>

            <p>A continuous path may include loops, and pass through the same city several times, but a given train may never be used twice in the same continuous path</p>

            <p>The <u>player with the most points</u> wins the game.</p>

            <p>If two or more players are tied with the most points, the player who has completed the most Destination Tickets is the winner</p>

            <p>In the unlikely event players are still tied, the player with the European Express bonus card wins</p>
        </section>
    )
}