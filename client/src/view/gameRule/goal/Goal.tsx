import styles from './Goal.module.css'
import boardImage from '../../../assets/game-rule/board.png'
import { Col, Row } from 'react-bootstrap'

export default function Goal() {
    return (
        <section className={ styles.section }>
            <h2>Goal of the Game</h2>
            <Row className='align-items-center'>
                <Col sm={ 12 }>
                    <p>The goal of the game is to is to score the highest total number of points. Points can be scored by</p>
                </Col>
                
                <Col sm={ 3 } className="text-left">
                    <ul>
                        <li>Building a rail line between two neighboring cities</li>
                        <li>Successfully completing a continuous path of routes between two cities listed on your Destination Ticket(s)</li>
                        <li>Completing the Longest Continuous Path of routes to win the European Express Bonus card</li>
                    </ul>
                </Col>

                <Col sm={ 9 }>
                    <img src={ boardImage } alt="Game Board" title="Game Board" />
                </Col>

                <Col sm={ 12 }>
                    <p>Points are deducted from the players' total score for each of their Destination Tickets that are not successfully completed by the end of the game.</p>
                </Col>
            </Row>
        </section>
    )
}