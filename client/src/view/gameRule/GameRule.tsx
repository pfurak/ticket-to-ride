import styles from './GameRule.module.css'
import { Button } from 'react-bootstrap'
import { useAppDispatch } from '../../state/store'
import { Pages, setView } from '../../state/view/action'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeftLong } from '@fortawesome/free-solid-svg-icons'
import { useEffect } from "react"
import AOS from 'aos'
import "aos/dist/aos.css"
import Goal from './goal/Goal'
import Actions from './actions/Actions'
import HowToPlay from './howToPlay/HowToPlay'
import Scoring from './scoring/scoring'

export default function GameRule() {
    const dispatch = useAppDispatch()

    const navigateBackToMainPage: React.MouseEventHandler<HTMLButtonElement> = () => {
        dispatch(setView(Pages.MAIN_PAGE))
    }

    useEffect(() => {
        AOS.init()
        AOS.refresh()

        document.querySelector<HTMLElement>('body')!.style.overflow = "auto"
        return(() => {
            document.querySelector<HTMLElement>('body')!.style.overflow = "hidden"
        })
    }, [])

    return (
        <div className={ styles.container }>
            <div className={ styles.containerBody }>
                <Button variant="success" onClick={ navigateBackToMainPage } className={ styles.homeButton }>
                    <FontAwesomeIcon icon={ faArrowLeftLong } /> Main Page
                </Button>

                <div data-aos="fade-right" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                    <Goal />
                </div>
                
                <div data-aos="fade-left" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                    <Actions />
                </div>

                <div data-aos="fade-right" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                    <HowToPlay />
                </div>

                <div data-aos="fade-left" data-aos-offset="300" data-aos-delay="50" data-aos-duration="500" data-aos-easing="ease-in-sine">
                    <Scoring />
                </div>
            </div>
        </div>
    )
}