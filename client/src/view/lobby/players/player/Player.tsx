import playerImage from '../../../../assets/player.png'
import styles from './Player.module.css'
import { Row, Col } from "react-bootstrap"
import { colorCodes } from '../../../game/exports'

export interface PlayerInterface {
    id: string,
    name: string,
    color: string,
    score: number,
    wagons: number
}

interface PropInterface {
    player: PlayerInterface
}

const Player: React.FC<PropInterface> = (props) => {
    const player = props.player
    const color = colorCodes[player.color]
    return (
        <Row className={ styles.playerContainer } style={{ backgroundColor: color }}>
            <Col sm={ 4 }>
                <img src={ playerImage } alt="Player" />
            </Col>

            <Col sm={ 8 }>
                { player.name }
            </Col>
        </Row>
    )
}

export default Player