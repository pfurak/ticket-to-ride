import { useEffect } from "react"
import { Row, Col } from "react-bootstrap"
import { subscribeToEvent, unSubscribeFromEvent } from "../../../socket"
import { socketEvents } from '../../../types/socket'
import { GameState, removePlayer, setPlayers } from "../../../state/game/action"
import { getPlayers } from "../../../state/game/selector"
import { useAppDispatch, useAppSelector } from "../../../state/store"
import Player, { PlayerInterface } from "./player/Player"
import { Pages, setView } from "../../../state/view/action"

const Players: React.FC = () => {
    const dispatch = useAppDispatch()
    const players = useAppSelector(getPlayers)

    useEffect(() => {
        subscribeToEvent(socketEvents.PLAYER_JOINED, (data: any) => {
            dispatch(setPlayers(data.players))
            if(data.gameState === GameState.IN_GAME) {
                dispatch(setView(Pages.GAME))
            }
        })

        subscribeToEvent(socketEvents.PLAYER_LEFT_ROOM, (data: any) => {
            const playerID = data.playerID
            dispatch(removePlayer(playerID))
        })

        return (() => {
            unSubscribeFromEvent([ socketEvents.PLAYER_JOINED, socketEvents.PLAYER_LEFT_ROOM ])
        })
    })

    return (
        <Row className="alig-items-center justify-content-center">
            {
                players.map((player: PlayerInterface) => {
                    return (
                        <Col sm={ 4 } key={ player.id }>
                            <Player player={ player } />
                        </Col>
                    )
                })
            }
        </Row>
    )
}

export default Players