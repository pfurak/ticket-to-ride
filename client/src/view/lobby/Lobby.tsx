import styles from './Lobby.module.css'
import music from '../../assets/lobby.mp3'
import { useEffect } from 'react'
import GameData from './gameData/GameData'
import Players from './players/Players'


const Lobby: React.FC = () => {
    useEffect(() => {
        const playMusic = process.env.REACT_APP_LOBBY_MUSIC === "true"
        if(playMusic) {
            const audio = new Audio(music)
            audio.loop = true
            audio.volume = 0.5
            const playPromise = audio.play()
    
            return(() => {
                if(playPromise !== undefined) {
                    playPromise.then(() => {
                        audio.pause();
                    })
                }
            })
        }
    })

    return (
        <div className={ styles.container }>
            <div className={ styles.containerBody }>
                <GameData />
                <Players />
            </div>
        </div>
    )
}

export default Lobby