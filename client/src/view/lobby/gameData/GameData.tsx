import { faSignOut } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Row, Col, Button } from 'react-bootstrap'
import { sendMessageToServer } from '../../../socket'
import { socketEndPoints, socketParams } from '../../../types/socket'
import { getGameID, getMyID, getRoomMaxPlayers } from '../../../state/game/selector'
import { resetState, useAppDispatch, useAppSelector } from '../../../state/store'

const GameData: React.FC = () => {
    const gameID = useAppSelector(getGameID)
    const roomMaxPlayers = useAppSelector(getRoomMaxPlayers)
    const myID = useAppSelector(getMyID)
    const dispatch = useAppDispatch()

    const handleLeaveRoomOnClick: React.MouseEventHandler<HTMLButtonElement> = () => {
        const params: socketParams = { gameID: gameID, playerID: myID }
        sendMessageToServer(socketEndPoints.LEAVE_ROOM, params, (data: any) => {
            if(data.success === true) {
                dispatch(resetState())
            }
        })
    }

    return (
        <Row className='align-items-center'>
            <Col sm={ 2 }>
                <Button variant="danger" onClick={ handleLeaveRoomOnClick }>
                    <FontAwesomeIcon icon={ faSignOut } /> Leave Room
                </Button>
            </Col>

            <Col sm={ 10 } className='text-right'>
                Room Code: { gameID }
                <br />
                Room Size: { roomMaxPlayers }
            </Col>
        </Row>
    )
}

export default GameData