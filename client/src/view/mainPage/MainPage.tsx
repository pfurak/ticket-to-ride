import styles from './MainPage.module.css'
import React, { useState } from "react"
import { Row, Col, Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChessBoard, faSearch, faTrain } from '@fortawesome/free-solid-svg-icons';
import { useAppDispatch } from '../../state/store';
import { Pages, setView } from '../../state/view/action';
import JoinRoom from './joinRoom/JoinRoom';
import CreateRoom from './createRoom/CreateRoom';

const MainPage: React.FC = () => {
    const [ joinRoomModal, setJoinRoomModal ] = useState(false)
    const [ createNewRoomModal, setCreateNewRoomModal ] = useState(false)
    const dispatch = useAppDispatch()

    const navigateToGameRuleScreen: React.MouseEventHandler<HTMLButtonElement> = () => {
        dispatch(setView(Pages.GAME_RULE))
    }

    const showJoinRoomModal: React.MouseEventHandler<HTMLButtonElement> = () => {
        if(!joinRoomModal) {
            setJoinRoomModal(true)
        }
    }

    const showCreateRoomModal: React.MouseEventHandler<HTMLButtonElement> = () => {
        if(!createNewRoomModal) {
            setCreateNewRoomModal(true)
        }
    }

    return (
        <div className={ styles.container }>
            <div className={ styles.containerBody }>
                <Row>
                    <Col sm={ 12 } className='text-center'>
                        <p>
                            Are you ready to build the longest railway in Europe?
                        </p>
                    </Col>
                </Row>

                <Row className='align-items-center justify-content-center'>
                    <Col sm={ 4 } className='text-center'>
                        <Button variant="warning" onClick={ navigateToGameRuleScreen }>
                            <FontAwesomeIcon icon={ faChessBoard } /> Game Rule
                        </Button>
                    </Col>

                    <Col sm={ 4 } className='text-center'>
                        <Button variant="primary" onClick={ showJoinRoomModal }>
                            <FontAwesomeIcon icon={ faSearch } /> Join Room
                        </Button>
                    </Col>

                    <Col sm={ 4 } className='text-center' onClick={ showCreateRoomModal }>
                        <Button variant="success">
                            <FontAwesomeIcon icon={ faTrain } /> Create Room
                        </Button>
                    </Col>
                </Row>
            </div>

            <JoinRoom isVisible={ joinRoomModal } hideModalCallback={ setJoinRoomModal } />
            <CreateRoom isVisible={ createNewRoomModal } hideModalCallback={ setCreateNewRoomModal } />
        </div>
    )
}

export default MainPage