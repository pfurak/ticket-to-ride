import {Modal, Alert, Fade, Button} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faUserFriends } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'
import { sendMessageToServer } from '../../../socket'
import { socketEndPoints, socketParams, socketCallbackErrorType, createRoomSocketCallbackParams } from '../../../types/socket'
import { useAppDispatch } from '../../../state/store'
import { GameState, setGameData } from '../../../state/game/action'
import { Pages, setView } from '../../../state/view/action'

type Props = {
    isVisible: boolean,
    hideModalCallback: React.Dispatch<React.SetStateAction<boolean>>
}

const CreateRoom: React.FC<Props> = ({ isVisible, hideModalCallback }) => {
    const localNickName: string = localStorage.getItem('nickName') ?? ''
    const MIN_PLAYERS: number = process.env.REACT_APP_MIN_PLAYERS ? parseInt(process.env.REACT_APP_MIN_PLAYERS) : 2
    const MAX_PLAYERS: number = process.env.REACT_APP_MAX_PLAYERS ? parseInt(process.env.REACT_APP_MAX_PLAYERS) : 5

    const [ nickName, setNickName ] = useState(localNickName)
    const [ roomSize, setRoomSize ] = useState(MIN_PLAYERS)
    const [ createRoomErrorMessage, setCreateRoomErrorMessage ] = useState('')

    const dispatch = useAppDispatch()

    const hideModal = () => {
        hideModalCallback(false)
        setNickName(localNickName)
        setRoomSize(MIN_PLAYERS)
        setCreateRoomErrorMessage('')
    }

    const handleCreateRoomClick: React.MouseEventHandler<HTMLButtonElement> = () => {
        let errorMessage: string = ""

        const nickNameInput = document.querySelector('#create-room-nickname')
        nickNameInput?.classList.remove('is-invalid')
        if(nickName === "") {
            nickNameInput?.classList.add('is-invalid')
            errorMessage += "Please add your Nickname"
        }

        const roomSizeInput = document.querySelector('#create-room-room-size')
        roomSizeInput?.classList.remove('is-invalid')
        if(isNaN(roomSize) || roomSize < MIN_PLAYERS || roomSize > MAX_PLAYERS) {
            roomSizeInput?.classList.add('is-invalid')
            if(errorMessage !== "") {
                errorMessage += "<br />"
            }
            errorMessage += "Please add the room size"
        }

        if(errorMessage === "") {
            localStorage.setItem('nickName', nickName)
            const params: socketParams = { maxPlayers: roomSize, playerName: nickName }
            sendMessageToServer(socketEndPoints.CREATE_ROOM, params, (data: socketCallbackErrorType | createRoomSocketCallbackParams) => {
                if(data.success === true && data.gameData) {
                    const gameData = data.gameData
                    dispatch(setGameData(gameData))
                    if(gameData.gameState === GameState.IN_LOBBY) {
                        dispatch(setView(Pages.LOBBY))
                    }
                }
                else {
                    if(data.errorMessage) {
                        setCreateRoomErrorMessage(data.errorMessage)
                    }
                }
            })
        }
        else {
            setCreateRoomErrorMessage(errorMessage)
        }
    }

    return (
        <Modal show={ isVisible } onHide={ hideModal } animation={ true }>
            <Modal.Header closeButton>
                <Modal.Title>Create Room</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text input-group-text-fa-icon" id="basic-addon1">
                            <FontAwesomeIcon icon={ faUser } />
                        </span>
                    </div>

                    <input
                        id="create-room-nickname"
                        type="text"
                        className="form-control"
                        placeholder="Nickname"
                        aria-label="Nickname"
                        aria-describedby="basic-addon1"
                        defaultValue={ localNickName }
                        onChange={ e => setNickName(e.target.value) }
                        />
                </div>

                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text input-group-text-fa-icon" id="basic-addon2">
                            <FontAwesomeIcon icon={ faUserFriends } />
                        </span>
                    </div>
                    <input
                        id="create-room-room-size"
                        type="number"
                        min={ MIN_PLAYERS }
                        max={ MAX_PLAYERS }
                        defaultValue={ roomSize }
                        className="form-control"
                        placeholder="Room Code"
                        aria-label="Room Code"
                        aria-describedby="basic-addon2"
                        onChange={ e => setRoomSize(parseInt(e.target.value)) }
                        />
                </div>

                <Alert
                    show={ createRoomErrorMessage.length > 0 }
                    transition={ Fade }
                    variant='danger'
                    onClose={() => setCreateRoomErrorMessage('') }
                    dismissible
                >
                    <span dangerouslySetInnerHTML={{__html: createRoomErrorMessage }}></span>
                </Alert>                    
                
            </Modal.Body>
            
            <Modal.Footer>
                <Button variant="secondary" onClick={ hideModal }>
                    Cancel
                </Button>
                
                <Button variant="primary" onClick={ handleCreateRoomClick }>
                    Create Room
                </Button>
            </Modal.Footer>
      </Modal>
    )
}

export default CreateRoom