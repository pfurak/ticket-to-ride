import {Modal, Alert, Fade, Button} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react';
import { sendMessageToServer } from '../../../socket'
import { socketEndPoints, socketParams, socketCallbackErrorType, joinRoomSocketCallbackParams } from '../../../types/socket'
import { GameState, setGameData } from '../../../state/game/action';
import { Pages, setView } from '../../../state/view/action';
import { useAppDispatch } from '../../../state/store';

type Props = {
    isVisible: boolean,
    hideModalCallback: React.Dispatch<React.SetStateAction<boolean>>
}

const JoinRoom: React.FC<Props> = ({ isVisible, hideModalCallback }) => {
    const localNickName: string = localStorage.getItem('nickName') ?? ''
    const [ nickName, setNickName ] = useState(localNickName)
    const [ roomCode, setRoomCode ] = useState('')
    const [ joinRoomErrorMessage, setJoinRoomErrorMessage ] = useState('')

    const dispatch = useAppDispatch()

    const hideModal = () => {
        hideModalCallback(false)
        setNickName(localNickName)
        setRoomCode('')
        setJoinRoomErrorMessage('')
    }

    const handleJoinRoomClick: React.MouseEventHandler<HTMLButtonElement> = () => {
        let errorMessage: string = ""

        const nickNameInput = document.querySelector('#join-room-nickname')
        nickNameInput?.classList.remove('is-invalid')
        if(nickName === "") {
            nickNameInput?.classList.add('is-invalid')
            errorMessage += "Please add your Nickname"
        }

        const roomCodeInput = document.querySelector('#join-room-room-code')
        roomCodeInput?.classList.remove('is-invalid')
        if(roomCode === "") {
            roomCodeInput?.classList.add('is-invalid')
            if(errorMessage !== "") {
                errorMessage += "<br />"
            }
            errorMessage += "Please add the room code"
        }

        if(errorMessage === "") {
            localStorage.setItem('nickName', nickName)
            const params: socketParams = { gameID: roomCode, playerName: nickName }
            sendMessageToServer(socketEndPoints.JOIN_ROOM, params, (data: joinRoomSocketCallbackParams | socketCallbackErrorType) => {
                if(data.success && data.gameData) {
                    const gameData = data.gameData
                    dispatch(setGameData(gameData))
                    if(gameData.gameState === GameState.IN_LOBBY) {
                        dispatch(setView(Pages.LOBBY))
                    }
                    else {
                        // GameState.inGame
                        dispatch(setView(Pages.GAME))
                    }
                }
                else {
                    if(data.errorMessage) {
                        setJoinRoomErrorMessage(data.errorMessage)
                    }
                }
            })
        }
        else {
            setJoinRoomErrorMessage(errorMessage)
        }
    }

    return (
        <Modal show={ isVisible } onHide={ hideModal } animation={ true }>
            <Modal.Header closeButton>
                <Modal.Title>Join Room</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text input-group-text-fa-icon" id="basic-addon1">
                            <FontAwesomeIcon icon={ faUser } />
                        </span>
                    </div>

                    <input
                        id="join-room-nickname"
                        type="text"
                        className="form-control"
                        placeholder="Nickname"
                        aria-label="Nickname"
                        aria-describedby="basic-addon1"
                        defaultValue={ localNickName }
                        onChange={ e => setNickName(e.target.value) }
                        />
                </div>

                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text input-group-text-fa-icon" id="basic-addon2">
                            <FontAwesomeIcon icon={ faLock } />
                        </span>
                    </div>
                    <input
                        id="join-room-room-code"
                        type="text"
                        className="form-control"
                        placeholder="Room Code"
                        aria-label="Room Code"
                        aria-describedby="basic-addon2"
                        onChange={ e => setRoomCode(e.target.value) }
                        />
                </div>

                <Alert
                    show={ joinRoomErrorMessage.length > 0 }
                    transition={ Fade }
                    variant='danger'
                    onClose={() => setJoinRoomErrorMessage('') }
                    dismissible
                >
                    <span dangerouslySetInnerHTML={{__html: joinRoomErrorMessage }}></span>
                </Alert>                    
                
            </Modal.Body>
            
            <Modal.Footer>
                <Button variant="secondary" onClick={ hideModal }>
                    Cancel
                </Button>
                
                <Button variant="primary" onClick={ handleJoinRoomClick }>
                    Join Room
                </Button>
            </Modal.Footer>
      </Modal>
    )
}

export default JoinRoom