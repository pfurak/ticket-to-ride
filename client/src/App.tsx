import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react'
import { useAppSelector } from './state/store'
import { getView } from './state/view/selector'
import { Pages } from './state/view/action'
import MainPage from './view/mainPage/MainPage'
import GameRule from './view/gameRule/GameRule';
import Lobby from './view/lobby/Lobby';
import Game from './view/game/Game';

export default function App() {
  const view = useAppSelector(getView)
  
  const getComponent = (view: Pages): React.ReactElement<any, any> | null => {
    if(view === Pages.GAME) {
      return <Game />
    }

    if(view === Pages.LOBBY) {
      return <Lobby />
    }

    if(view === Pages.GAME_RULE) {
      return <GameRule />
    }

    return <MainPage />
  }

  return getComponent(view)
}