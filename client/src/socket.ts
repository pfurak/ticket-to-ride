import io from "socket.io-client"
import { socketEndPoints, socketEvents, socketCallback } from './types/socket'

const socketIOServer: string = process.env.REACT_APP_SOCKET_IO_SERVER || ''
const socket = io(socketIOServer)

socket?.on('disconnect', function() {
    if(typeof(socket) !== "undefined") {
        socket.open()
    }
})

export const sendMessageToServer = (endPoint: socketEndPoints, parameters: any, callback: socketCallback) => {
    if(!socket.connected) {
        socket.connect()
        console.log(socket.connected)
        if(!socket.connected) {
            callback({ success: false, errorMessage: "Cannot connect to server" })
            return
        }
    }
    
    if(process.env.REACT_APP_SOCKET_IO_LOG_EVENTS === "true") {
        console.log("sendMessageToServer", endPoint, parameters)
    }
    socket?.emit(endPoint, parameters, callback)
}

export const subscribeToEvent = (event: socketEvents, callback: socketCallback) => socket?.on(event, callback)
export const unSubscribeFromEvent = (events: socketEvents | Array<socketEvents>) => {
    if(!Array.isArray(events)) {
        events = [ events ]
    }
    if(process.env.REACT_APP_SOCKET_IO_LOG_EVENTS === "true") {
        console.log("unSubscribeFromEvent", events)
    }
    events.forEach(event => socket?.off(event))
}