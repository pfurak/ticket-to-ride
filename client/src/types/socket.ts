import { historyInterface } from "../state/game/reducer"

export enum socketEndPoints {
    CREATE_ROOM = "CREATE_ROOM",
    JOIN_ROOM = "JOIN_ROOM",
    LEAVE_ROOM = "LEAVE_ROOM",
    GET_GAME_DATA = "GET_GAME_DATA",
    CHOOSE_TICKETS = "CHOOSE_TICKETS",
    SEND_ACTION = "SEND_ACTION"
}

export enum actionTypes {
    ADD_TURN = "ADD_TURN",
    DRAW_TICKET = "DRAW_TICKET",
    DRAW_UPSIDED_CARD = "DRAW_UPSIDED_CARD",
    DRAW_CARD_FROM_DECK = "DRAW_CARD_FROM_DECK",
    BUILD = "BUILD",
    GAME_ENDED = "GAME_ENDED"
}

export enum socketEvents {
    ADD_TURN = "ADD_TURN",
    PLAYER_JOINED = "PLAYER_JOINED",
    PLAYER_LEFT_ROOM = "PLAYER_LEFT_ROOM",
    PLAYER_CHOOSED_TICKETS = "PLAYER_CHOOSED_TICKETS",
    GET_ACTION = "GET_ACTION",
    GAME_ENDED = "GAME_ENDED"
}

export interface socketParams {
    maxPlayers?: number,
    playerName?: string
    gameID?: string,
    playerID?: string
    choosedTickets?: Array<number>
}

interface gameData {
    gameID: string,
    gameState: string,
    maxPlayers: number,
    players: Array<any>,
    registeredPlayerID: string
}

export interface socketCallbackErrorType {
    success: boolean,
    errorMessage: string,
    gameData?: gameData
}

export interface createRoomSocketCallbackParams {
    success: boolean,
    gameData: gameData,
    errorMessage?: string
}

export interface joinRoomSocketCallbackParams {
    success: boolean,
    gameData: gameData,
    errorMessage?: string
}

export interface playerCoosedTicketsSocketCallbackParams {
    success: boolean,
    ticketDeckSize: number,
    history: Array<historyInterface>,
    player: Object,
    currentPlayerID?: string
}

export type socketCallback = (data: any) => void