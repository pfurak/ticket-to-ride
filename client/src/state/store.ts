import { combineReducers } from 'redux'
import { configureStore } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import { gameReducer } from './game/reducer'
import { viewReducer } from './view/reducer'
import { pinReducer } from './game/pin/reducer'
import { actionReducer } from './action/reducer'
import { possibleBuildLinesReducer } from './game/possibleBuildLines/reducer'

const RESET_STATE = 'RESET_STATE'

const appReducer = combineReducers({
    view: viewReducer,
    game: gameReducer,
    pin: pinReducer,
    possibleBuildLines: possibleBuildLinesReducer,
    action: actionReducer
})

const rootReducer = (state: any, action: any) => {
    if(action.type === RESET_STATE) {
        state = undefined
    }
    return appReducer(state, action)
}

export const store = configureStore({
    reducer: rootReducer,
    devTools: process.env.NODE_ENV !== 'production'
})

export const resetState = () => ({
    type: RESET_STATE
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector