import { SET_VIEW, Pages } from './action'

const initialState = Pages.MAIN_PAGE

interface Action {
    type: string,
    payload?: string
}

export const viewReducer = (state: any = initialState, action: Action) => {
    const { type, payload } = action

    if(type === SET_VIEW) {
        return payload
    }

    return state
}