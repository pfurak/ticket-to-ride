export const SET_VIEW : string = "SET_VIEW"

export enum Pages {
    MAIN_PAGE = "MAIN_PAGE",
    GAME_RULE = "GAME_RULE",
    LOBBY = "LOBBY",
    GAME = "GAME"
}

export const setView = (view: Pages) => ({
    type: SET_VIEW,
    payload: view,
})