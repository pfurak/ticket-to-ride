import { REGISTER_ACTION, REMOVE_ACTION } from "./action"

let actionCounter = 0
const initialState: Array<Object> = []

export const actionReducer = (state: any = initialState, action: any) => {
    const { type, payload } = action

    if(type === REGISTER_ACTION) {
        return [ ...state, {
            ...payload,
            id: actionCounter++
        }]
    }

    if(type === REMOVE_ACTION) {
        const id = payload
        return state.filter((action: any) => action.id !== id)
    }

    return state
}