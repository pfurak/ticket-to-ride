export const REGISTER_ACTION : string = "REGISTER_ACTION"
export const REMOVE_ACTION : string = "REMOVE_ACTION"

export const registerAction = (data: any) => ({
    type: REGISTER_ACTION,
    payload: data
})

export const removeAction = (id: any) => ({
    type: REMOVE_ACTION,
    payload: id
})
