export const SET_VISIBLE_PINS = 'SET_VISIBLE_PINS'
export const REMOVE_FROM_VISIBLE_PINS = 'REMOVE_FROM_VISIBLE_PINS'
export const RESET_VISIBLE_PINS = 'RESET_VISIBLE_PINS'

export enum pinAddedWith {
    ON_HOVER,
    ON_CLICK
}

export interface stateInterface {
    [id: number]: pinAddedWith
}

export const setVisiblePins = (pins: stateInterface) => ({
    type: SET_VISIBLE_PINS,
    payload: pins,
})

export const removeFromVisiblePins = (pins: stateInterface) => ({
    type: REMOVE_FROM_VISIBLE_PINS,
    payload: pins,
}) 

export const resetVisiblePins = () => ({
    type: RESET_VISIBLE_PINS,
    payload: {}
})