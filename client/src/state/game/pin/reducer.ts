import { SET_VISIBLE_PINS, REMOVE_FROM_VISIBLE_PINS, stateInterface, RESET_VISIBLE_PINS } from './action'

interface Action {
    type: string,
    payload?: stateInterface
}

const initialState: stateInterface = {}

export const pinReducer = (state: any = initialState, action: Action) => {
    const { type, payload } = action

    if(payload === undefined) {
        return state
    }

    if(type === SET_VISIBLE_PINS) {
        const copiedState = {...state}
        const newPins = payload
        Object.keys(newPins).map(x => parseInt(x)).forEach((pin: number) => {
            if(!copiedState[pin]) {
                copiedState[pin] = newPins[pin]
            }
        })
        
        return copiedState
    }

    if(type === REMOVE_FROM_VISIBLE_PINS) {
        const copiedState = {...state}
        const removablePins = payload
        Object.keys(removablePins).map(x => parseInt(x)).forEach((pin: number) => {
            if(copiedState[pin] === removablePins[pin]) {
                delete copiedState[pin]
            }
        })
        return copiedState
    }

    if(type === RESET_VISIBLE_PINS) {
        return payload
    }

    return state
}