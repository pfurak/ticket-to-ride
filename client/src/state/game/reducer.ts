import { SET_GAME_DATA, GameState, SET_PLAYERS, REMOVE_PLAYER, playerState, PLAYER_CHOOSE_TICKET, MODIFY_TICKET_DECK_SIZE, MODIFY_PLAYER, ADD_TURN, MODIFY_UPSIDED_TRAIN_CARDS, DRAW_UPSIDED_TRAIN_CARDS, MODIFY_DECK_SIZE, DRAW_CARD_FROM_DECK, MODIFY_RAIL_LINES, BUILD, GAME_ENDED } from "./action"

interface playerInterface {
    id: string,
    color: string,
    name: string,
    finishedTickets: number,
    numberOfCards: number,
    numberOfTickets: number,
    score: number,
    wagons: number
    state?: playerState
}

export interface historyInterface {
    playerName: string
    colorCode: string
    text: string
}

interface Action {
    type: string,
    payload: any
}

const initialState = {
    gameID: "",
    maxPlayers: 0,
    myID: "",
    players: "",
    currentPlayerID: "",
    gameState: GameState.IN_LOBBY,
    ticketDeckSize: 0,
    desckSize: 0,
    upsidedTrainCards: [],
    railLines: [],
    history: [],
    orderedPlayers: []
}

export const gameReducer = (state: any = initialState, action: Action) => {
    const { type, payload } = action    

    if(payload === undefined) {
        return state
    }

    if(type === SET_GAME_DATA) {
        const myID = payload.registeredPlayerID ?? state.myID
        const players: Array<playerInterface> = payload.players
        players.forEach((player: any, index) => {
            if(player.id === myID) {
                players[index].state = player.state
            }
        })

        return {
            gameID: payload.gameID,
            maxPlayers: payload.maxPlayers,
            myID: myID,
            players: players,
            currentPlayerID: payload.currentPlayerID ?? state.currentPlayerID,
            gameState: payload.gameState,
            ticketDeckSize: payload.ticketDeckSize ?? state.ticketDeckSize,
            desckSize: payload.desckSize ?? state.desckSize,
            upsidedTrainCards: payload.upsidedTrainCards ?? state.upsidedTrainCards,
            railLines: payload.railLines ?? state.railLines,
            history: payload.history ?? state.history,
            orderedPlayers: payload.orderedPlayers ?? state.orderedPlayers
        }
    }

    if(type === SET_PLAYERS) {
        return {
            ...state,
            players: payload
        }
    }

    if(type === REMOVE_PLAYER) {
        const playerID = payload
        if(state.gameState === GameState.IN_LOBBY) {
            const players = state.players.filter((player: any) => player.id !== playerID)
            return {
                ...state,
                players: players
            }
        }
        else if(state.gameState === GameState.IN_GAME) {
            return {
                ...state,
                players: state.players.map((player: playerInterface) => player.id !== playerID ? player : { ...player, state: playerState.LEFT_GAME })
            }
        }
        else {
            return state
        }
    }

    if(type === PLAYER_CHOOSE_TICKET) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const ticketDeckSize = payload.ticketDeckSize
        const player: playerInterface = payload.player
        const history = payload.history
        const players = state.players.map((statePlayer: playerInterface) => statePlayer.id === player.id ? player : statePlayer)
        return {
            ...state,
            ticketDeckSize: ticketDeckSize,
            players: players,
            history: history
        }
    }

    if(type === MODIFY_TICKET_DECK_SIZE) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const ticketDeckSize = payload
        return {
            ...state,
            ticketDeckSize: ticketDeckSize
        }
    }

    if(type === MODIFY_PLAYER) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const player: playerInterface = payload
        if(player) {
            return {
                ...state,
                players: state.players.map((statePlayer: playerInterface) => statePlayer.id === player.id ? player : statePlayer)
            }
        }
    }

    if(type === ADD_TURN) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const currentPlayerID = payload
        const players: Array<playerInterface> = []
        
        state.players.forEach((player: playerInterface) => {
            let playerNewState = player.state
            if(playerNewState !== playerState.LEFT_GAME) {
                playerNewState = player.id === currentPlayerID ? playerState.WAIT_FOR_ACTION : playerState.WAIT_FOR_TURN
            }
            players.push({
                ...player,
                state: playerNewState
            })
        })
        
        return {
            ...state,
            currentPlayerID: currentPlayerID,
            players: players
        }
    }

    if(type === MODIFY_UPSIDED_TRAIN_CARDS) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const upsidedTrainCards = payload
        return {
            ...state,
            upsidedTrainCards: upsidedTrainCards
        }
    }

    if(type === DRAW_UPSIDED_TRAIN_CARDS) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const { upsidedTrainCards, desckSize, history, player } = payload
        const players: Array<playerInterface> = state.players.map((statePlayer: playerInterface) => player.id === statePlayer.id ? player : statePlayer)
        
        return {
            ...state,
            upsidedTrainCards: upsidedTrainCards,
            desckSize: desckSize,
            history: history,
            players: players
        }
    }

    if(type === MODIFY_DECK_SIZE) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const desckSize = payload
        return {
            ...state,
            desckSize: desckSize
        }
    }

    if(type === DRAW_CARD_FROM_DECK) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        const { desckSize, history, player } = payload
        const players: Array<playerInterface> = state.players.map((statePlayer: playerInterface) => player.id === statePlayer.id ? player : statePlayer)
        
        return {
            ...state,
            desckSize: desckSize,
            history: history,
            players: players
        }
    }

    if(type === MODIFY_RAIL_LINES) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }

        return {
            ...state,
            railLines: payload
        }
    }

    if(type === BUILD) {
        if(state.gameState !== GameState.IN_GAME) {
            return state
        }
        
        const { desckSize, history, player, railLines, upsidedTrainCards } = payload
        const players: Array<playerInterface> = state.players.map((statePlayer: playerInterface) => player.id === statePlayer.id ? player : statePlayer)

        return {
            ...state,
            desckSize: desckSize,
            history: history,
            players: players,
            railLines: railLines,
            upsidedTrainCards: upsidedTrainCards
        }
    }

    if(type === GAME_ENDED) {
        const orderedPlayers = payload
        return {
            ...state,
            gameState: GameState.ENDED_GAME,
            orderedPlayers: orderedPlayers
        }
    }

    return state
}