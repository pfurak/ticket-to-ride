import { connectionsInterface } from '../../view/game/exports'
import { historyInterface } from './reducer'

export const SET_GAME_DATA = "SET_GAME_DATA"
export const SET_PLAYERS = "SET_PLAYERS"
export const REMOVE_PLAYER = "REMOVE_PLAYER"
export const PLAYER_CHOOSE_TICKET = "PLAYER_CHOOSE_TICKET"
export const MODIFY_TICKET_DECK_SIZE = "MODIFY_TICKET_DECK_SIZE"
export const MODIFY_PLAYER = "MODIFY_PLAYER"
export const ADD_TURN = "ADD_TURN"
export const MODIFY_UPSIDED_TRAIN_CARDS = "MODIFY_UPSIDED_TRAIN_CARDS"
export const DRAW_UPSIDED_TRAIN_CARDS = "DRAW_UPSIDED_TRAIN_CARDS"
export const MODIFY_DECK_SIZE = "MODIFY_DECK_SIZE"
export const DRAW_CARD_FROM_DECK = "DRAW_CARD_FROM_DECK"
export const MODIFY_RAIL_LINES = "MODIFY_RAIL_LINES"
export const BUILD = "BUILD"
export const GAME_ENDED = "GAME_ENDED"

export enum GameState {
    IN_LOBBY = "IN_LOBBY",
    IN_GAME = "IN_GAME",
    ENDED_GAME = "ENDED_GAME"
}

export enum playerState {
    CHOOSE_TICKET = "CHOOSE_TICKET",
    DRAW_SECOND_CARD = "DRAW_SECOND_CARD",
    DRAW_SECOND_CARD_FROM_DECK = "DRAW_SECOND_CARD_FROM_DECK",
    CHOOSE_CARD_VARIANT_TO_BUILD = "CHOOSE_CARD_VARIANT_TO_BUILD",
    BUILD = "BUILD",
    WAIT_FOR_TURN = "WAIT_FOR_TURN",
    WAIT_FOR_ACTION = "WAIT_FOR_ACTION",
    LEFT_GAME = "LEFT_GAME" 
}

export const setGameData = (gameData: Object) => ({
    type: SET_GAME_DATA,
    payload: gameData,
})

export const setPlayers = (players: Object) => ({
    type: SET_PLAYERS,
    payload: players,
})

export const removePlayer = (playerID: string) => ({
    type: REMOVE_PLAYER,
    payload: playerID
})

export const playerChooseTicket = (ticketDeckSize: number, player: Object, history: Array<historyInterface>) => ({
    type: PLAYER_CHOOSE_TICKET,
    payload: { 
        ticketDeckSize: ticketDeckSize,
        player: player,
        history: history
    }
})

export const modifyTicketDeckSize = (ticketDeckSize: number) => ({
    type: MODIFY_TICKET_DECK_SIZE,
    payload: ticketDeckSize
})

export const modifyPlayer = (player: Object) => ({
    type: MODIFY_PLAYER,
    payload: player
})

export const addTurn = (playerID: string) => ({
    type: ADD_TURN,
    payload: playerID
})

export const modifyUpsidedTrainCards = (cards: Object) => ({
    type: MODIFY_UPSIDED_TRAIN_CARDS,
    payload: cards
})

export const drawUpsidedTrainCard = (upsidedTrainCards: Object, desckSize: number, history: Array<historyInterface>, player: Object) => ({
    type: DRAW_UPSIDED_TRAIN_CARDS,
    payload: {
        upsidedTrainCards: upsidedTrainCards,
        desckSize: desckSize,
        history: history,
        player: player
    }
})

export const modifyDeckSize = (desckSize: number) => ({
    type: MODIFY_DECK_SIZE,
    payload: desckSize
})

export const drawCardFromDeck = (desckSize: number, history: Array<historyInterface>, player: Object) => ({
    type: DRAW_CARD_FROM_DECK,
    payload: {
        desckSize: desckSize,
        history: history,
        player: player
    }
})

export const modifyRailLines = (railLines: Array<connectionsInterface>) => ({
    type: MODIFY_RAIL_LINES,
    payload: railLines
})

export const build = (desckSize: number, history:  Array<historyInterface>, player: Object, railLines: Array<connectionsInterface>, upsidedTrainCards: Object) => ({
    type: BUILD,
    payload: {
        desckSize: desckSize,
        history: history,
        player: player,
        railLines: railLines,
        upsidedTrainCards: upsidedTrainCards
    }
})

export const gameEnded = (orderedPlayers: Object) => ({
    type: GAME_ENDED,
    payload: orderedPlayers
})