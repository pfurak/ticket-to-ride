import { connectionsInterface } from "../../view/game/exports"
import { GameState } from "./action"

export const getGameState = (state: any): GameState => state.game.gameState
export const getGameID = (state: any) => state.game.gameID
export const getRoomMaxPlayers = (state: any) => state.game.maxPlayers
export const getMyID = (state: any) => state.game.myID
export const getPlayers = (state: any) => state.game.players
export const getPlayer = (state: any) => state.game.players.find((player: any) => player.id === state.game.myID)
export const getCurrentPlayer = (state: any) => state.game.players.find((player: any) => player.id === state.game.currentPlayerID)
export const getOtherPlayers  = (state: any) => state.game.players.filter((player: any) => player.id !== state.game.myID)
export const getTicketDeckSize = (state: any) => state.game.ticketDeckSize
export const getUpsidedTrainCards = (state: any) => state.game.upsidedTrainCards
export const getDeckSize = (state: any) => state.game.desckSize
export const getRailLines = (state: any): Array<connectionsInterface> => state.game.railLines
export const getHistory = (state: any) => state.game.history
export const getOrderedPlayers = (state: any) => state.game.orderedPlayers