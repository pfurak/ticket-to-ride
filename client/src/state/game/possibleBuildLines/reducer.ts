import { RESET_POSSIBLE_BUILD_LINES, SET_POSSIBLE_BUILD_LINES } from "./action"

const initialState: Array<number> = []

export const possibleBuildLinesReducer = (state = initialState, action: any) => {
    const { type, payload } = action

    if(payload === undefined) {
        return state
    }

    if(type === RESET_POSSIBLE_BUILD_LINES || type === SET_POSSIBLE_BUILD_LINES) {
        return payload
    }

    return state
}