export const RESET_POSSIBLE_BUILD_LINES = 'RESET_POSSIBLE_BUILD_LINES'
export const SET_POSSIBLE_BUILD_LINES = 'SET_POSSIBLE_BUILD_LINES'

export const resetPossibleBuildLines = () => ({
    type: RESET_POSSIBLE_BUILD_LINES,
    payload: []
})

export const setPossibleBuildLines = (lines: Array<number>) => ({
    type: SET_POSSIBLE_BUILD_LINES,
    payload: lines
})