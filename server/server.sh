#!/bin/bash

clear

BASE_PATH="$(cd "$( dirname "$0" )" && pwd)/"
SERVER_PATH="${BASE_PATH}dist/"
SERVER_MAIN_FILE="server.js"
COMMAND="node"

######################################################################
# Print usage information
######################################################################
function usage () {
	echo server.sh
	echo Usage:
	echo "    server.sh <command>"
	echo Commands:
	echo "    start     start up the server"
	echo "    stop      stop the server"
	echo "    restart":
	echo "      if not running, start up the server"
	echo "      if running, stop and start up the server"
}

function getServerPID() {
    echo $(ps aux | grep "${SERVER_PATH}${SERVER_MAIN_FILE}" | grep -v "grep" | awk '{print $2}')
}

######################################################################
# Start up the server
######################################################################
function start() {
    SERVER_PID=$(getServerPID)
    if [ -z "$SERVER_PID" ]
    then
        cd "${SERVER_PATH}"

        ${COMMAND} "${SERVER_PATH}${SERVER_MAIN_FILE}" &
        clear
        echo "Server started..."
        cd "${BASE_PATH}"
    else
        echo "Server already running..."
    fi
}

######################################################################
# Stop the server
######################################################################
function stop() {
    IS_RESTART="$1"
    SERVER_PID=$(getServerPID)
    if [ ! -z "$SERVER_PID" ]
    then
        for i in ${SERVER_PID}
        do
            #echo "${i}"
            kill -SIGTERM ${i}
        done
        echo "Stop the server..."
    else
        if [ -z "$IS_RESTART" ] || [ "$IS_RESTART" != true ]
        then
            echo "Server not running..."
        fi
    fi
}

######################################################################
# Check the number of parameters, exit when <> 1
######################################################################
if [ $# -ne 1 ]
then
   usage
   exit 1
fi

######################################################################
# Start up the server
######################################################################
if [ "$1" = "start" ]
then
   start
   exit 0
fi

######################################################################
# Stop the server
######################################################################
if [ "$1" = "stop" ]
then
   stop
   exit 0
fi

######################################################################
# Stop the server
######################################################################
if [ "$1" = "restart" ]
then
   stop true
   start
   exit 0
fi

usage
exit 1