export enum cardType {
    PURPLE = "purple",
    WHITE = "white",
    BLUE = "blue",
    YELLOW = "yellow",
    ORANGE = "orange",
    BLACK = "black",
    RED = "red",
    GREEN = "green",
    JOKER = "joker"
}