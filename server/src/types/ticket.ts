export enum ticketType {
    LONG = "LONG",
    SHORT = "SHORT"
}

export interface ticket {
    id: number,
    from: number,
    to: number,
    fromCity: string,
    toCity: string,
    point: number,
}