import Game from "../game"

export enum socketEndPoints {
    CREATE_ROOM = "CREATE_ROOM",
    JOIN_ROOM = "JOIN_ROOM",
    LEAVE_ROOM = "LEAVE_ROOM",
    GET_GAME_DATA = "GET_GAME_DATA",
    CHOOSE_TICKETS = "CHOOSE_TICKETS",
    SEND_ACTION = "SEND_ACTION"
}

export enum socketEmitters {
    PLAYER_JOINED = "PLAYER_JOINED",
    PLAYER_LEFT_ROOM  = "PLAYER_LEFT_ROOM",
    ADD_TURN = "ADD_TURN",
    PLAYER_CHOOSED_TICKETS = "PLAYER_CHOOSED_TICKETS",
    GET_ACTION = "GET_ACTION",
    GAME_ENDED = "GAME_ENDED"
}

export interface gameRoomInterface {
    [id: string]: Game
}

export interface createRoomParameter {
    maxPlayers: number,
    playerName: string
}

export interface joinRoomParameter {
    gameID: string,
    playerName: string
}

export interface leaveRoomParameter {
    gameID: string,
    playerID: string
}

export interface chooseTicketsParameter {
    gameID: string,
    playerID: string,
    choosedTickets: Array<number>
}

export interface sendActionParameters {
    gameID: string,
    playerID: string,
    action: string,
    otherParams?: Object
}