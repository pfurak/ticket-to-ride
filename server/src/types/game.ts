import { connectionsInterface } from "../assets/connections"
import Card from "../card"
import History from "../history"
import Player from "../player"
import { cardType } from "./card"

export enum gameState {
    IN_LOBBY = "IN_LOBBY",
    IN_GAME = "IN_GAME",
    ENDED_GAME = "ENDED_GAME"
}

export enum actionTypes {
    DRAW_TICKET = "DRAW_TICKET",
    DRAW_UPSIDED_CARD = "DRAW_UPSIDED_CARD",
    DRAW_CARD_FROM_DECK = "DRAW_CARD_FROM_DECK",
    BUILD = "BUILD"
}

export interface playerInterface {
    [id: string]: Player
}

export interface registerPlayerInterface {
    gameID: string,
    maxPlayers: number,
    gameState: gameState,
    registeredPlayerID: string,
    players: Array<any>
}

export interface handleActionInterface {
    success: boolean,
    errorMessage?: string,
    animate?: string,
    afterAnimate?: {
        player: Object,
        playerToOthers?: Object
    }
}

export interface handleTicketDrawInterface {
    success: boolean,
    animatedCardCounter: number,
    animateCaller: string,
    afterAnimate: {
        ticketDeckSize: number,
        player: Object
    }
}

export interface handleDrawUpsidedCardInterface {
    success: boolean,
    upsidedTrainCards?: Array<Card>
    animateCaller?: string,
    upsidedCardID?: number,
    cardTypeFromDeck?: cardType,
    afterAnimate?: {
        upsidedTrainCards: Array<Card>,
        desckSize: number,
        player: Object,
        playerToOthers: Object,
        history: Array<History>
    }
}

export interface handleDrawCardFromDeckInterface {
    success: boolean,
    desckSize?: number,
    animateCaller?: string,
    cardTypeFromDeck?: cardType,
    afterAnimate?: {
        desckSize: number,
        player: Object,
        playerToOthers: Object,
        history: Array<History>
    }
}

export interface playerChooseTicketResult {
    ticketDeckSize: number,
    history: Array<History>,
    playerData: Object,
    extendedPlayerData: Object
}

export interface handleDrawUpsidedCardParams {
    cardID: number
}

export interface handleBuildParams {
    railLineID: number,
    cards: {
        [cardType: string]: number
    }
}

export interface handleBuildInterface {
    success: boolean,
    railLines?: Array<connectionsInterface>,
    player?: Object,
    animateCaller?: string,
    animateCallerColor?: string,
    railLineID?: number,
    railLineCarNumbers?: number,
    afterAnimate?: {
        desckSize: number,
        upsidedTrainCards: Array<Card>,
        player: Object,
        playerToOthers: Object,
        railLines?: Array<connectionsInterface>,
        history: Array<History>
    }
}