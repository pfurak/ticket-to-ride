import { config as dotenvConfig } from 'dotenv'
import SocketIOServer from "./socket"

const main = async () => {
    /*
    const access = fs.createWriteStream('api.access.log');
    process.stdout.write = process.stderr.write = access.write.bind(access);
    */
    const port: number = parseInt(process.env.SERVER_PORT) || 3001
    const socket = new SocketIOServer(port)
    socket.listen()

    const signals : string[] = ['SIGTERM', 'SIGINT']
    signals.forEach(signal => {
        process.on(signal, () => {
            socket.close()
            process.exit(0)
        })
    })

    process.on('uncaughtException', err => console.error((err && err.stack) ? err.stack : err))
}

dotenvConfig()
main()