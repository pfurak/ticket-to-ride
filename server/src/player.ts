import { connectionsInterface } from "./assets/connections"
import Card from "./card"
import Functions from "./functions"
import Ticket from "./ticket"
import { playerState } from "./types/player"

class Player {
    private id: string
    private name: string
    private color: string
    private score: number
    private wagons: number
    private cards: Array<Card>
    private longTicket: Ticket
    private choosableTickets: Array<Ticket>
    private tickets: Array<Ticket>
    private playerState: playerState
    private turnCounter: number
    private firstTimeToChooseTicket: boolean
    private railLines: Array<connectionsInterface>
    private longestRailLine: Array<number>
    private longestRailLineScore: number
    private summedPoints: number
    private hasTheLongestRailLine: boolean

    constructor(id: string, name: string, color: string) {
        this.id = id
        this.name = name
        this.color = color
        this.score = 0
        this.wagons = parseInt(process.env.DEFAULT_WAGONS) || 45
        this.cards = []
        this.longTicket = null
        this.choosableTickets = []
        this.tickets = []
        this.turnCounter = 0
        this.firstTimeToChooseTicket = true
        this.railLines = []
        this.longestRailLine = []
        this.longestRailLineScore = 0
        this.summedPoints = 0
        this.hasTheLongestRailLine = false
    }

    getID(): string {
        return this.id
    }

    getName(): string {
        return this.name
    }

    getColorCode() {
        return this.color
    }

    getScrore(): number {
        return this.score
    }

    increaseScore(score : number) {
        this.score += score
    }

    getWagons(): number {
        return this.wagons
    }

    isFirstTimeToChooseTicket() {
        return this.firstTimeToChooseTicket
    }

    modifyTicketDrawLogical() {
        this.firstTimeToChooseTicket = false
    }

    increaseTurnCounter() {
        this.turnCounter++
    }

    getTurnCounter(): number {
        return this.turnCounter
    }

    decreaseWagons(wagon : number) {
        this.wagons -= wagon
    }

    getPlayerState() {
        return this.playerState
    }

    setPlayerState(state: playerState) {
        if(this.playerState !== playerState.LEFT_GAME) {
            this.playerState = state
        }
    }

    inGame(): boolean {
        return this.playerState !== playerState.LEFT_GAME
    }

    getLongestRailLineScore() {
        return this.longestRailLineScore
    }

    addLongestRailLineScoreToSummedPoints() {
        this.hasTheLongestRailLine = true
        this.summedPoints += 10
    }

    getSummedPoints() {
        return this.summedPoints
    }

    getFinishedTicketsNumber() {
        let counter = this.longTicket.completed() ? 1 : 0
        counter += this.tickets.filter(ticket => ticket.completed()).length
        return counter
    }

    hasEuropeExpress() {
        return this.hasTheLongestRailLine
    }

    addCard(card: Card) {
        if(card instanceof Card) {
            this.cards.push(card)
        }
    }

    addLongTicket(ticket: Ticket) {
        if(ticket instanceof Ticket) {
            this.longTicket = ticket
        }
    }

    addTicket(ticket: Ticket) {
        if(ticket instanceof Ticket) {
            this.choosableTickets.push(ticket)
        }
    }

    chooseTicket(ticketIDs: Array<number>): Array<Ticket> {
        const discardedTickets: Array<Ticket> = []
        this.choosableTickets.forEach((ticket: Ticket) => {
            if(ticketIDs.includes(ticket.getID())) {
                this.tickets.push(ticket)
            }
            else {
                discardedTickets.push(ticket)
            }
        })
        this.choosableTickets = []
        this.checkIfTicketsCompleted()

        return discardedTickets
    }

    build(railLine: connectionsInterface, buildScore: number, cardVariants: {[cardType: string]: number }): Array<Card> {
        if(this.wagons >= railLine.cars.length) {
            const copiedCards = [ ...this.cards ].reverse()
            const discardedCards = []
            let maxCard = 0

            Object.keys(cardVariants).forEach(key => {
                maxCard += cardVariants[key]
                let i = 0
                while(i < copiedCards.length && cardVariants[key] > 0) {
                    if(copiedCards[i].getType() === key) {
                        discardedCards.push(copiedCards[i])
                        copiedCards.splice(i, 1)
                        cardVariants[key]--
                        i--
                    }
                    i++
                }
            })

            if(maxCard === discardedCards.length) {
                this.cards = copiedCards.reverse()
    
                this.decreaseWagons(railLine.cars.length)
                this.increaseScore(buildScore)

                this.railLines.push(railLine)

                this.checkIfTicketsCompleted()
                this.calculateLongestRailLine()

                return discardedCards
            }
        }
    }

    checkIfTicketsCompleted() {
        if(this.railLines.length < 2) {
            return false
        }

        if(!this.longTicket.completed()) {
            if(this.longTicket.checkIfCompleted(this.railLines)) {
                this.increaseScore(this.longTicket.getPoint())
            }
        }

        this.tickets.forEach(ticket => {
            if(!ticket.completed()) {
                if(ticket.checkIfCompleted(this.railLines)) {
                    this.increaseScore(ticket.getPoint())
                }
            }
        })
    }
    
    calculateLongestRailLine() {
        if(this.railLines.length > 0) {
            if(this.railLines.length === 1) {
                this.longestRailLine = [ this.railLines[0].id ]
                this.longestRailLineScore = this.railLines[0].cars.length
            }
            else {
                this.railLines.forEach(line => this.setLongestRailLine(line))
            }
        }
    }

    setLongestRailLine(line: connectionsInterface, parentNeighbours: Array<number> = [], currentTree: Array<number> = []) {
        const tree = [ ...currentTree, line.id]
        const lineNeighbours = Functions.getRailLineNeighbours(this.railLines, line)
        const checkAbleNeighbours = lineNeighbours.filter(line => !currentTree.includes(line.id) && !parentNeighbours.includes(line.id))
        if(checkAbleNeighbours.length === 0) {
            let newTreeScrore = 0
            this.railLines.forEach(line => {
                if(tree.includes(line.id)) {
                    newTreeScrore += line.cars.length
                }
            })
            if(newTreeScrore > this.longestRailLineScore) {
                this.longestRailLine = tree
                this.longestRailLineScore = newTreeScrore
            }
        }
        else {
            const lineNeighboursIDs = checkAbleNeighbours.map(line => line.id)
            checkAbleNeighbours.forEach(neighbour => this.setLongestRailLine(neighbour, lineNeighboursIDs, tree))
        }
    }

    calculateSummedPoints() {
        this.summedPoints = this.score
        if(!this.longTicket.completed()) {
            this.summedPoints -= this.longTicket.getPoint()
        }

        this.tickets.forEach(ticket => {
            if(!ticket.completed()) {
                this.summedPoints -= ticket.getPoint()
            }
        })
    }

    toObject(extended: boolean = false): Object {
        let numberOfTickets = this.tickets.length
        let finishedTickets = 0

        this.tickets.forEach((ticket: Ticket) => {
            if(ticket.completed()) {
                finishedTickets++
            }
        })

        if(this.longTicket instanceof Ticket) {
            numberOfTickets++

            if(this.longTicket.completed()) {
                finishedTickets++
            }
        }

        const obj = {
            id: this.id,
            name: this.name,
            color: this.color,
            score: this.score,
            wagons: this.wagons,
            numberOfCards: this.cards.length,
            numberOfTickets: numberOfTickets,
            finishedTickets: finishedTickets,
            longestRailLine: this.longestRailLine,
            longestRailLineScore: this.longestRailLineScore
        }

        if(extended) {
            obj['state'] = this.playerState
            obj['tickets'] = [this.longTicket, ...this.tickets]
            obj['choosableTickets'] = this.choosableTickets
            obj['cards'] = {}
            this.cards.forEach((card: Card) => {
                const cardType = card.getType()
                if(!obj['cards'][cardType]) {
                    obj['cards'][cardType] = 0
                }

                obj['cards'][cardType]++
            })
        }

        return obj
    }
}

export = Player