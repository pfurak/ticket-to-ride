import { createServer, Server } from "http"
import { Server as SocketServer } from "socket.io"
import { socketEndPoints, socketEmitters, createRoomParameter, joinRoomParameter, leaveRoomParameter, chooseTicketsParameter, sendActionParameters, gameRoomInterface } from './types/socket'
import Game from "./game"
import { playerChooseTicketResult } from "./types/game"
import Functions from "./functions"

const MAX_PLAYERS : number = parseInt(process.env.MAX_PLAYERS) ?? 5

class SocketIOServer {
    private server: Server
    private port: number
    private games: gameRoomInterface = {}

    constructor(port: number) {
        this.port = port
    }

    listen() {
        this.server = createServer()
        const io = new SocketServer(this.server, {
            cors: {
                origin: "*",
                methods: ["GET", "POST"]
            },
            allowEIO3: true
        })

        io.on('connection', socket => {
            // Create new game room
            socket.on(socketEndPoints.CREATE_ROOM, (parameters: createRoomParameter, ack) => {
                const maxPlayers: number = parameters.maxPlayers
                if(maxPlayers > MAX_PLAYERS) {
                    ack({ success: false, errorMessage: "Maximum number of players is: " + MAX_PLAYERS })
                }
                else {
                    let gameID = Functions.generateRandomString()
                    while(this.games[gameID]) {
                        gameID = Functions.generateRandomString()
                    }

                    this.games[gameID] = new Game(gameID, maxPlayers)
                    const gameData = this.games[gameID].registerPlayer(parameters.playerName)
                    if(gameData) {
                        socket.join(gameID)
                        ack({ success: true, gameData: gameData })
                    }
                    else {
                        delete this.games[gameID]
                        ack({ success: false, errorMessage: "Cannot register player" })
                    }
                }
            })

            // Join to existing game room
            socket.on(socketEndPoints.JOIN_ROOM, (parameters: joinRoomParameter, ack) => {
                const gameID = parameters.gameID
                if(this.games[gameID]) {
                    if(!this.games[gameID].roomIsFull()) {
                        const gameData = this.games[gameID].registerPlayer(parameters.playerName)
                        if(gameData) {
                            socket.join(gameID)
                            socket.to(gameID).emit(socketEmitters.PLAYER_JOINED, { gameState: gameData.gameState, players: gameData.players })
                            ack({ success: true, gameData: gameData })
                        }
                        else {
                            ack({ success: false, errorMessage: "Cannot register player" })
                        }
                    }
                    else {
                        ack({ success: false, errorMessage: "Room is full" })
                    }
                }
                else {
                    ack({ success: false, errorMessage: "Cannot find room with ID: " + gameID })
                }
            })

            // Leave from an existing game room
            socket.on(socketEndPoints.LEAVE_ROOM, (parameters: leaveRoomParameter, ack) => {
                const gameID = parameters.gameID
                if(this.games[gameID]) {
                    const playerID = parameters.playerID
                    if(this.games[gameID].hasPlayerWithID(playerID)) {
                        this.games[gameID].removePlayer(playerID)

                        socket.leave(gameID)
                        ack({ success: true })

                        if(this.games[gameID].roomIsEmpty()) {
                            delete this.games[gameID]
                        }
                        else {
                            socket.broadcast.to(gameID).emit(socketEmitters.PLAYER_LEFT_ROOM, { playerID: playerID })
                            if(this.games[gameID].gameEnded()) {
                                const orderedPlayers = this.games[gameID].getOrderedPlayers()
                                io.to(gameID).emit(socketEmitters.GAME_ENDED, { orderedPlayers: orderedPlayers })
                            }
                            else {
                                if(this.games[gameID].turnAdded()) {
                                    const currentPlayerID = this.games[gameID].getCurrentPlayerID()
                                    io.to(gameID).emit(socketEmitters.ADD_TURN, { currentPlayerID: currentPlayerID })
                                }
                            }

                        }
                    }
                    else {
                        ack({ success: false, errorMessage: "Cannot find player in room with ID: " + playerID })
                    }
                }
                else {
                    ack({ success: false, errorMessage: "Cannot find room with ID: " + gameID })
                }
            })

            socket.on(socketEndPoints.GET_GAME_DATA, (parameters, ack) => {
                const gameID = parameters.gameID

                if(this.games[gameID]) {
                    const playerID = parameters.playerID
                    const gameData = this.games[gameID].getGameData(playerID)
                    ack({ success: true, gameData: gameData })
                }
                else {
                    ack({ success: false, errorMessage: "Cannot find room with ID: " + gameID })
                }
            })

            // Player choose ticket
            socket.on(socketEndPoints.CHOOSE_TICKETS, (parameters: chooseTicketsParameter, ack) => {
                const gameID = parameters.gameID
                if(this.games[gameID]) {
                    const result: playerChooseTicketResult = this.games[gameID].playerChooseTicket(parameters.playerID, parameters.choosedTickets)
                    socket.broadcast.to(gameID).emit(socketEmitters.PLAYER_CHOOSED_TICKETS, { ticketDeckSize: result.ticketDeckSize, history: result.history, player: result.playerData })

                    ack({ success: true, ticketDeckSize: result.ticketDeckSize, history: result.history, player: result.extendedPlayerData })

                    if(this.games[gameID].turnAdded()) {
                        const currentPlayerID = this.games[gameID].getCurrentPlayerID()
                        io.to(gameID).emit(socketEmitters.ADD_TURN, { currentPlayerID: currentPlayerID })
                    }
                    else {
                        if(this.games[gameID].gameEnded()) {
                            const orderedPlayers = this.games[gameID].getOrderedPlayers()
                            io.to(gameID).emit(socketEmitters.GAME_ENDED, { orderedPlayers: orderedPlayers })
                        }
                    }
                }
                else {
                    ack({ success: false, errorMessage: "Cannot find room with ID: " + gameID })
                }
            })

            socket.on(socketEndPoints.SEND_ACTION, (parameters: sendActionParameters, ack) => {
                const gameID = parameters.gameID
                if(this.games[gameID]) {
                    const result = this.games[gameID].handleAction(parameters.playerID, parameters.action, parameters.otherParams)
                    ack(result)

                    if(result.success === true) {
                        delete result['success']
                        if(result.afterAnimate && result.afterAnimate.playerToOthers) {
                            result.afterAnimate.player = result.afterAnimate.playerToOthers
                            delete result.afterAnimate.playerToOthers
                        }
                        socket.broadcast.to(gameID).emit(socketEmitters.GET_ACTION, result)
                        if(this.games[gameID].turnAdded()) {
                            const currentPlayerID = this.games[gameID].getCurrentPlayerID()
                            io.to(gameID).emit(socketEmitters.ADD_TURN, { currentPlayerID: currentPlayerID })
                        }
                        else {
                            if(this.games[gameID].gameEnded()) {
                                const orderedPlayers = this.games[gameID].getOrderedPlayers()
                                io.to(gameID).emit(socketEmitters.GAME_ENDED, { orderedPlayers: orderedPlayers })
                            }
                        }
                    }
                }
                else {
                    ack({ success: false, errorMessage: "Cannot find room with ID: " + gameID })
                }
            })
        })

        this.server.listen(this.port, () => {
            console.log("Socket.io server has started on port: " + this.port)
        })
    }

    close() {
        this.server.close()
        console.log("Socket.io server shut down")
    }
}

export = SocketIOServer