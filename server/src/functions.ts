import fs from 'fs'
import util  from "util"
import { connectionsInterface } from "./assets/connections"

class Functions {
    public static generateRandomString(length: number = 10) {
        let result = ''
        const characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        const charactersLength = characters.length
        for(let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength))
        }
        return result
    }

    public static getRailLineNeighbours = (railLines: Array<connectionsInterface>, currentRailLine: connectionsInterface) => {
        const currentLineID = currentRailLine.id
        const currentLinePins = [currentRailLine.from, currentRailLine.to]
        return railLines.filter(line => line.id !== currentLineID && (currentLinePins.includes(line.from) || currentLinePins.includes(line.to)))
    }

    public static async debugLog(message: string | Object) {
        const msg = typeof message === "string" ? message : util.inspect(message, {showHidden: false, depth: null, colors: false})
        await fs.promises.appendFile('debug.log', msg + "\n")
    }
}

export = Functions