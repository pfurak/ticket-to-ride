import Deck from './deck'
import Card from './card'
import Player from './player'
import { actionTypes, gameState, handleActionInterface, handleBuildInterface, handleBuildParams, handleDrawCardFromDeckInterface, handleDrawUpsidedCardInterface, handleDrawUpsidedCardParams, handleTicketDrawInterface, playerChooseTicketResult, playerInterface, registerPlayerInterface } from './types/game'
import TicketDeck from './ticketDeck'
import { ticketType } from './types/ticket'
import { playerState } from './types/player'
import Ticket from './ticket'
import { cardType } from './types/card'
import History from './history'
import { historyEnum } from './types/history'
import { connections, connectionsInterface } from './assets/connections'
import Functions from './functions'

class Game {
    private static maxChoosableTicketCounter = 3
    private static initGamePlayerCardCounter = 4
    private static maxUpsidedTrainCards = 5
    private static maxJokerInUpsidedTrainCards = 3
    private static maxUpsidedCardsShuffleCounter = 3
    private static playerColors = ["green", "red", "yellow", "blue", "purple"]
    private static railwayLinePoints = { 1: 1, 2: 2, 3: 4, 4: 7, 6: 15, 8: 21 }
    private id: string
    private maxPlayers: number
    private players: playerInterface = {}
    private gameState: gameState
    private isTurnAdded: boolean
    private currentPlayer: number
    private deck: Deck
    private upsidedTrainCards: Array<Card> = []
    private longTicketDeck: TicketDeck
    private ticketDeck: TicketDeck
    private railLines: Array<connectionsInterface> = []
    private history: Array<History> = []
    private lastTurnTriggeredBy: string
    private orderedPlayers: Array<Player> = []
    private nexTurnIsGameEnd: boolean = false

    constructor(id: string, maxPlayers: number) {
        this.id = id
        this.maxPlayers = maxPlayers
        this.gameState = gameState.IN_LOBBY
    }

    roomIsFull() : boolean {
        return Object.keys(this.players).length === this.maxPlayers
    }

    roomIsEmpty() : boolean {
        return Object.keys(this.players).length === 0
    }

    hasPlayerWithID(id: string) : boolean {
        return Object.keys(this.players).includes(id)
    }

    registerPlayer(playerName: string) : registerPlayerInterface {
        if(!this.roomIsFull()) {
            let playerID = Functions.generateRandomString()
            while(this.players[playerID]) {
                playerID = Functions.generateRandomString()
            }

            const color = Game.playerColors[Object.keys(this.players).length]
            this.players[playerID] = new Player(playerID, playerName, color)

            if(this.roomIsFull()) {
                this.gameState = gameState.IN_GAME
                this.initGame()
            }

            const result = { gameID: this.id, maxPlayers: this.maxPlayers, gameState: this.gameState, registeredPlayerID: playerID, players: [] }
            for(const [_, player] of Object.entries(this.players)) {
                result.players.push(player.toObject())
            }

            return result
        }
    }

    removePlayer(playerID: string) {
        if(this.hasPlayerWithID(playerID)) {
            if(this.gameState === gameState.IN_GAME) {
                this.players[playerID].setPlayerState(playerState.LEFT_GAME)
                const playerIDs = Object.keys(this.players)
                const inGamePlayers = playerIDs.filter(playerID => this.players[playerID].inGame())
                if(inGamePlayers.length === 1) {
                    this.setEndedGame()
                }
                else {
                    if(playerID === this.getCurrentPlayerID()) {
                        this.addTurn()
                    }
                }
            }
            else {
                delete this.players[playerID]
            }
        }
    }

    getCurrentPlayerID(): string {
        const playerIDs = Object.keys(this.players)
        return playerIDs[this.currentPlayer]
    }

    gameEnded() {
        return this.gameState === gameState.ENDED_GAME
    }

    getOrderedPlayers() {
        return this.orderedPlayers
    }

    initGame() {
        this.currentPlayer = 0 // index of Object.keys(this.players)
        this.lastTurnTriggeredBy = null // Store playerID into this variable

        this.longTicketDeck = new TicketDeck(ticketType.LONG)
        this.ticketDeck = new TicketDeck(ticketType.SHORT)
        this.deck = new Deck()
        this.railLines = connections.map(x => x) // Need to copy data

        Object.keys(this.players).map((key: string) => {
            this.players[key].setPlayerState(playerState.CHOOSE_TICKET)
            this.players[key].addLongTicket(this.longTicketDeck.drawCard())

            for(let i = 0; i < Game.maxChoosableTicketCounter; i++) {
                this.players[key].addTicket(this.ticketDeck.drawCard())
            }

            for(let i = 0; i < Game.initGamePlayerCardCounter; i++) {
                this.players[key].addCard(this.deck.drawCard())
            }
        })
        this.setUpsidedTrainCards()
    }

    setUpsidedTrainCards(tryCounter = 0) {
        if(this.deck.getNumberOfCards() > 0 || this.deck.resetDeckFromDiscard()) {
            while(this.deck.getNumberOfCards() > 0 && this.upsidedTrainCards.length < Game.maxUpsidedTrainCards) {
                const card = this.deck.drawCard()
                if(card) {
                    this.upsidedTrainCards.push(card)
                }
            }

            if(this.upsidedTrainCards.length > 0) {
                this.handleIfUpsidedCardsContainsMoreJoker(tryCounter)
            }
        }
    }

    handleIfUpsidedCardsContainsMoreJoker(tryCounter = 0) {
        if(tryCounter < Game.maxUpsidedCardsShuffleCounter) {
            const numberOfJoker = this.upsidedTrainCards.filter(card => card.getType() === cardType.JOKER).length
            if(numberOfJoker >= Game.maxJokerInUpsidedTrainCards) {
                this.deck.addToDiscardDeck(this.upsidedTrainCards)
                this.upsidedTrainCards = []
                this.setUpsidedTrainCards(++tryCounter)
            }
        }
    }

    getGameData(playerID: string): Object {
        const playerIDs = Object.keys(this.players)
        const currentPlayerID = playerIDs[this.currentPlayer]
        const gameData = {
            gameID: this.id,
            maxPlayers: playerIDs.length,
            gameState: this.gameState,
            ticketDeckSize: this.ticketDeck.getNumberOfCards(),
            desckSize: this.deck.getNumberOfCards(),
            upsidedTrainCards: this.upsidedTrainCards,
            currentPlayerID: currentPlayerID,
            players: [],
            railLines: this.railLines,
            history: this.history
        }

        playerIDs.forEach(id => gameData.players.push( this.players[id].toObject(id === playerID) ))

        return gameData
    }

    playerChooseTicket(playerID: string, ticketIDs: Array<number>): playerChooseTicketResult {
        this.isTurnAdded = false

        const discardedPlayerTickets: Array<Ticket> = this.players[playerID].chooseTicket(ticketIDs)
        discardedPlayerTickets.forEach((ticket: Ticket) => {
            this.ticketDeck.addTicket(ticket)
        })

        this.history.push(new History(this.players[playerID].getColorCode(), this.players[playerID].getName(), historyEnum.DRAW_TICKET))

        if(this.players[playerID].isFirstTimeToChooseTicket()) {
            this.players[playerID].modifyTicketDrawLogical()

            //Someone builded while I choose ticket so server already added the turn, but on the client I need to see the animate
            const playerIDs = Object.keys(this.players)
            const currentPlayerID = playerIDs[this.currentPlayer]
            this.players[playerID].setPlayerState(playerID === currentPlayerID ? playerState.WAIT_FOR_ACTION : playerState.WAIT_FOR_TURN)
        }
        else {
            this.addTurn()
        }

        return {
            ticketDeckSize: this.ticketDeck.getNumberOfCards(),
            history: this.history,
            playerData: this.players[playerID].toObject(),
            extendedPlayerData: this.players[playerID].toObject(true)
        }
    }

    handleAction(playerID: string, action: string, otherParams): handleActionInterface {
        this.isTurnAdded = false

        if(!this.players[playerID]) {
            return {
                success: false,
                errorMessage: "Cannot find player with id: " + playerID
            }
        }

        if(action === actionTypes.DRAW_TICKET) {
            return {
                animate: action,
                ...this.handleTicketDraw(playerID)
            }
        }

        if(action === actionTypes.DRAW_UPSIDED_CARD) {
            return {
                animate: action,
                ...this.handleDrawUpsidedCard(playerID, otherParams)
            }
        }

        if(action === actionTypes.DRAW_CARD_FROM_DECK) {
            return {
                animate: action,
                ...this.handleDrawCardFromDeck(playerID)
            }
        }
        
        if(action === actionTypes.BUILD) {
            return {
                animate: action,
                ...this.handleBuild(playerID, otherParams)
            }
        }

        return {
            success: false,
            errorMessage: "Unhandled action: " + action
        }
    }

    handleTicketDraw(playerID: string): handleTicketDrawInterface {
        let i = 0
        while(i < Game.maxChoosableTicketCounter && this.ticketDeck.getNumberOfCards() > 0) {
            this.players[playerID].addTicket(this.ticketDeck.drawCard())
            i++
        }

        this.players[playerID].setPlayerState(playerState.CHOOSE_TICKET)

        return {
            success: true,
            animatedCardCounter: i,
            animateCaller: playerID,
            afterAnimate: {
                ticketDeckSize: this.ticketDeck.getNumberOfCards(),
                player: this.players[playerID].toObject(true)
            }
        }
    }

    handleDrawUpsidedCard(playerID: string, otherParams: handleDrawUpsidedCardParams): handleDrawUpsidedCardInterface {
        const id = otherParams.cardID
        const card = this.upsidedTrainCards.find(card => card.getID() === id)
        if(card) {
            this.upsidedTrainCards = this.upsidedTrainCards.filter(card => card.getID() !== id)
            this.players[playerID].addCard(card)
            const cardFromDeck = this.deck.drawCard()
            if(cardFromDeck) {
                this.upsidedTrainCards.push(cardFromDeck)
            }
    
            this.history.push(new History(this.players[playerID].getColorCode(), this.players[playerID].getName(), historyEnum.DRAW_CARD))
    
            if(this.upsidedTrainCards) {
                this.handleIfUpsidedCardsContainsMoreJoker()
            }

            // There is no card in upsided cards and there is no card in deck
            // need to add the turn here
            if(card.getType() === cardType.JOKER || this.players[playerID].getPlayerState() === playerState.DRAW_SECOND_CARD || (this.deck.getNumberOfCards() === 0 && this.upsidedTrainCards.length === 0)) {
                this.addTurn()
            }
            else {
                // draw all of the cards and deck only contains joker...
                if(this.upsidedTrainCards.length === this.upsidedTrainCards.filter(card => card.getType() === cardType.JOKER).length) {
                    this.addTurn()
                }
                else {
                    this.players[playerID].setPlayerState(playerState.DRAW_SECOND_CARD) 
                }
            }

            const result = {
                success: true,
                animateCaller: playerID,
                upsidedCardID: id,
                cardTypeFromDeck: null,
                afterAnimate: {
                    upsidedTrainCards: this.upsidedTrainCards,
                    desckSize: this.deck.getNumberOfCards(),
                    player: this.players[playerID].toObject(true),
                    playerToOthers: this.players[playerID].toObject(),
                    history: this.history
                }
            }
    
            if(cardFromDeck) {
                result.cardTypeFromDeck = cardFromDeck.getType()
            }
            return result
        }
        else {
            return {
                success: false,
                upsidedTrainCards: this.upsidedTrainCards
            }
        }
    }

    handleDrawCardFromDeck(playerID: string): handleDrawCardFromDeckInterface {
        if(this.deck.getNumberOfCards() > 0) {
            const card = this.deck.drawCard()
            this.players[playerID].addCard(card)            
            this.history.push(new History(this.players[playerID].getColorCode(), this.players[playerID].getName(), historyEnum.DRAW_CARD))

            if(this.players[playerID].getPlayerState() === playerState.WAIT_FOR_ACTION) {
                if(this.deck.getNumberOfCards() === 0) {
                    this.addTurn()
                }
                else {
                    this.players[playerID].setPlayerState(playerState.DRAW_SECOND_CARD_FROM_DECK)
                }
            }
            else {
                this.addTurn()
            }

            return {
                success: true,
                animateCaller: playerID,
                cardTypeFromDeck: card.getType(),
                afterAnimate: {
                    desckSize: this.deck.getNumberOfCards(),
                    player: this.players[playerID].toObject(true),
                    playerToOthers: this.players[playerID].toObject(),
                    history: this.history
                }
            }
        }
        else {
            return {
                success: false,
                desckSize: 0
            }
        }
    }

    handleBuild(playerID: string, otherParams: handleBuildParams): handleBuildInterface {
        const railLine = this.railLines.find(railLine => railLine.id === otherParams.railLineID)
        if(!railLine || railLine.selled || !railLine.enabled) {
            return {
                success: false,
                railLines: this.railLines
            }
        }

        const result = this.players[playerID].build(railLine, Game.railwayLinePoints[railLine.cars.length], otherParams.cards)
        if(!result) {
            return {
                success: false,
                railLines: this.railLines,
                player: this.players[playerID].toObject(true)
            }
        }

        this.deck.addToDiscardDeck(result)
        this.deck.resetDeckFromDiscard()
        this.setUpsidedTrainCards()

        this.railLines = this.railLines.map(line => line.id === railLine.id ? { ...line, selled: true, color: this.players[playerID].getColorCode() } : line)
        if(this.maxPlayers < 3) {
            const pins = [railLine.from, railLine.to]
            this.railLines = this.railLines.map(line => line.id !== railLine.id && pins.includes(line.from) && pins.includes(line.to) ? { ...line, enabled: false } : line)
        }

        this.history.push(new History(this.players[playerID].getColorCode(), this.players[playerID].getName(), historyEnum.BUILD))
        this.addTurn()

        if(this.players[playerID].getWagons() < 3) {
            this.lastTurnTriggeredBy = playerID
        }

        return {
            success: true,
            animateCaller: playerID,
            animateCallerColor: this.players[playerID].getColorCode(),
            railLineID: railLine.id,
            railLineCarNumbers: railLine.cars.length,
            afterAnimate: {
                desckSize: this.deck.getNumberOfCards(),
                upsidedTrainCards: this.upsidedTrainCards,
                player: this.players[playerID].toObject(true),
                playerToOthers: this.players[playerID].toObject(),
                railLines: this.railLines,
                history: this.history
            }
        }
    }

    addTurn() {
        if(this.nexTurnIsGameEnd) {
            this.setEndedGame()
        }
        else {
            const playerIDs = Object.keys(this.players)
            let currentPlayerID = playerIDs[this.currentPlayer]
            this.players[currentPlayerID].setPlayerState(playerState.WAIT_FOR_TURN)
            this.players[currentPlayerID].increaseTurnCounter()
            
            // We gonna check "if only one player inGame" in removePlayer
            this.currentPlayer++
            if(this.currentPlayer >= playerIDs.length) {
                this.currentPlayer = 0
            }
            currentPlayerID = playerIDs[this.currentPlayer]
    
            while(!this.players[currentPlayerID].inGame()) {
                this.currentPlayer++
                if(this.currentPlayer >= playerIDs.length) {
                    this.currentPlayer = 0
                }
                currentPlayerID = playerIDs[this.currentPlayer]
            }
    
            if(this.lastTurnTriggeredBy === currentPlayerID) {
                this.nexTurnIsGameEnd = true
            }

            this.isTurnAdded = true
            this.players[currentPlayerID].setPlayerState(playerState.WAIT_FOR_ACTION)
        }
    }

    turnAdded(): boolean {
        return this.isTurnAdded
    }

    setEndedGame() {
        this.gameState = gameState.ENDED_GAME
        let longestRailLineScore = 0
        Object.keys(this.players).forEach(playerID => {
            this.players[playerID].calculateSummedPoints()

            // If there is a tie between several players for the longest route, each of them will receive 10 points for the Europa Express.
            longestRailLineScore = Math.max(longestRailLineScore, this.players[playerID].getLongestRailLineScore())
        })

        Object.keys(this.players).forEach(playerID => {
            if(longestRailLineScore === this.players[playerID].getLongestRailLineScore()) {
                this.players[playerID].addLongestRailLineScoreToSummedPoints()
            }
            this.orderedPlayers.push(this.players[playerID])
        })

        this.orderedPlayers.sort((a, b) => {
            if(a.getSummedPoints() > b.getSummedPoints()) {
                return -1
            }
            else if(b.getSummedPoints() > a.getSummedPoints()) {
                return 1
            }
            else {
                const aFinishedTickets = a.getFinishedTicketsNumber()
                const bFinishedTickets = b.getFinishedTicketsNumber()
                if(aFinishedTickets > bFinishedTickets) {
                    return -1
                }
                else if(bFinishedTickets > aFinishedTickets) {
                    return 1
                }
                else {
                    const aHasEuropeExpress = a.hasEuropeExpress()
                    const bHasEuropeExpress = b.hasEuropeExpress()
                    if(aHasEuropeExpress && !bHasEuropeExpress) {
                        return -1
                    }
                    else if(bHasEuropeExpress && !aHasEuropeExpress) {
                        return 1
                    }
                    else {
                        return 0
                    }
                }
            }
        })
    }
}

export = Game