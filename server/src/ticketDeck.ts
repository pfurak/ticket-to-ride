import longTickets from "./assets/longtickets"
import tickets from "./assets/tickets"
import { ticketType, ticket } from "./types/ticket"
import Ticket from "./ticket"

class TicketDeck {
    private deck: Array<Ticket> = []
    private type: ticketType

    constructor(type: ticketType) {
        this.type = type
        this.setUpDeck()
        this.suffleDeck()
    }

    private setUpDeck() {
        const ticketArray = this.type === ticketType.LONG ? longTickets : tickets

        ticketArray.forEach((ticket: ticket) => this.deck.push(new Ticket(ticket.id, ticket.from, ticket.to, ticket.fromCity, ticket.toCity, ticket.point, this.type)))
    }

    suffleDeck() {
        const newDeck: Array<Ticket> = []
        while(this.deck.length > 0) {
            const index = Math.floor(Math.random() * this.deck.length)
            newDeck.push(this.deck[index])
            this.deck.splice(index, 1)
        }
        this.deck = newDeck
    }

    drawCard(): Ticket {
        if(this.deck.length > 0) {
            return this.deck.shift()
        }
    }

    getNumberOfCards() : number {
        return this.deck.length
    }

    addTicket(ticket: Ticket) {
        this.deck.push(ticket)
    }
}

export = TicketDeck