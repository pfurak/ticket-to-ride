import { historyEnum } from "./types/history"

class History {
    private playerName: string
    private colorCode: string
    private text: string

    constructor(colorCode: string, playerName: string, textEnum: historyEnum) {
        this.colorCode = colorCode
        this.playerName = playerName
        this.text = textEnum
    }
}

export = History