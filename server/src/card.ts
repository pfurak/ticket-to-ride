import { cardType } from "./types/card"

class Card {
    private id: number
    private type: cardType

    constructor(id: number, type: cardType) {
        this.id = id
        this.type = type
    }

    getID() : number {
        return this.id
    }

    getType() : cardType {
        return this.type
    }
}

export = Card