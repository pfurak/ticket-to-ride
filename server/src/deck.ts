import Card from "./card"
import { cardType } from "./types/card"

class Deck {
    private static cardTypeCounter = 12
    private static increaseJokerCounter = 2
    private static cards: Object = {
        purple: Deck.cardTypeCounter,
        white: Deck.cardTypeCounter,
        blue: Deck.cardTypeCounter,
        yellow: Deck.cardTypeCounter,
        orange: Deck.cardTypeCounter,
        black: Deck.cardTypeCounter,
        red: Deck.cardTypeCounter,
        green: Deck.cardTypeCounter,
        joker: Deck.cardTypeCounter + Deck.increaseJokerCounter
    }

    private deck: Array<Card> = []
    private discardedCards: Array<Card> = []

    constructor() {
        this.initDeck()
    }

    private initDeck() {
        const defaultDeck = {...Deck.cards} // need to copy
        const cardTypes = Object.keys(defaultDeck)
        const defaultDeckLength = cardTypes.length
        let numberOfCards = 0
        cardTypes.map((key: string) => numberOfCards += defaultDeck[key])

        while(this.deck.length < numberOfCards) {
            let type = cardTypes[Math.floor(Math.random() * defaultDeckLength)];
            while(defaultDeck[type] === 0) {
                type = cardTypes[Math.floor(Math.random() * defaultDeckLength)];
            }
            this.deck.push(new Card(this.deck.length + 1, cardType[type.toUpperCase()]))
            defaultDeck[type]--;
        }
    }

    drawCard(): Card {
        if(this.deck.length > 0) {
            const card = this.deck.shift()
            if(this.deck.length === 0) {
                this.resetDeckFromDiscard()
            }
            return card
        }
        else {
            if(this.resetDeckFromDiscard()) {
                return this.drawCard()
            }
        }
    }

    getNumberOfCards() : number {
        return this.deck.length
    }

    addToDiscardDeck(cards: Card | Array<Card>) {
        if(!Array.isArray(cards)) {
            cards = [ cards ]
        }

        this.discardedCards = [ ...this.discardedCards, ...cards ]
    }

    resetDeckFromDiscard() : boolean {
        if(this.deck.length === 0 && this.discardedCards.length > 0) {
            while(this.discardedCards.length > 0) {
                const index = Math.floor(Math.random() * this.discardedCards.length)
                this.deck.push(this.discardedCards[index])
                this.discardedCards.splice(index, 1)
            }
            return true
        }
        return false
    }
}

export = Deck