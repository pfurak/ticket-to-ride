import { connectionsInterface } from "./assets/connections"
import Functions from "./functions"
import { ticketType } from "./types/ticket"

class Ticket {
    private id: number
    private from: number
    private to: number
    private fromCity: string
    private toCity: string
    private point: number
    private type: ticketType
    private isCompleted: boolean
    private railways: Array<number>

    constructor(id: number, from: number, to: number, fromCity: string, toCity: string, point: number, type: ticketType) {
        this.id = id
        this.from = from
        this.to = to
        this.toCity = toCity
        this.fromCity = fromCity
        this.point = point
        this.type = type
        this.isCompleted = false
        this.railways = []
    }

    getID() : number {
        return this.id
    }

    getPoint() : number {
        return this.point
    }

    completed(): boolean {
        return this.isCompleted
    }

    checkIfCompleted(railLines: Array<connectionsInterface>) {
        if(!this.isCompleted) {
            const ticketFromLines = railLines.filter(line => line.from === this.from || line.to === this.from)
            const ticketToLines = railLines.filter(line => line.from === this.to || line.to === this.to).map(line => line.id)

            if(ticketFromLines.length > 0 && ticketToLines.length > 0) {
                let i = 0
                while(i < ticketFromLines.length && !this.isCompleted) {
                    this.buildConnectionTree(railLines, ticketFromLines.map(line => line.id), ticketToLines, ticketFromLines[i])
                    i++
                }
            }
        }
        return this.isCompleted
    }

    private buildConnectionTree(playerRailLines: Array<connectionsInterface>, ticketFromLines: Array<number>, ticketToLines: Array<number>, fromLine: connectionsInterface, parentNeighbours: Array<number> = [], currentTree: Array<number> = []) {
        if(ticketToLines.includes(fromLine.id)) {
            this.railways = [ ...currentTree, fromLine.id]
            this.isCompleted = true
        }
        else {
            const lineNeighbours = Functions.getRailLineNeighbours(playerRailLines, fromLine)
            const lineNeighboursIDs = lineNeighbours.map(line => line.id)
            // Functions.debugLog({ ticket: {from: this.fromCity, to: this.toCity}, currenTree: currentTree, parentNeighbours: parentNeighbours, currentLine: { ...fromLine, lineNeighbours: lineNeighbours} })
            
            const tree = [ ...currentTree, fromLine.id]
            let i = 0
            while(i < lineNeighbours.length && !this.isCompleted) {
                if(
                    !ticketFromLines.includes(lineNeighbours[i].id) // Start point, don't need to check that
                    && !currentTree.includes(lineNeighbours[i].id) // This will be a circle
                    && !parentNeighbours.includes(lineNeighbours[i].id) // parent gonna check that line too so we dont need to check here
                ) {
                    this.buildConnectionTree(playerRailLines, ticketFromLines, ticketToLines, lineNeighbours[i], lineNeighboursIDs, tree)
                }
                i++
            }
        }
    }
}

export = Ticket